# AeTrapp Mobile

Mobile application to collect samples and monitor local Aedes Aegypti populations.

Check our [contributing guide](CONTRIBUTING.md).

#### About the samples cycles

After a trap is set, the user should wait the opening of sample collecting window. This happens on the 7th day, when the user can send a sample picture. The window closes on the 8th day, when the trap starts to have a delayed status. If the user do not send a picture, the trap is deactivated automatically at the 10th day.

## Developing

Node.js 8+ and React Native are required.

### Clone locally and install modules

    git clone <repository_url>
    cd <repository_path>
    yarn

### Create a config file

Create a config.json file from the example:

    cp config.example.json config.json

Set the endpoint to the AeTrapp API:

```
{
  "API_URL": "http://localhost:3030"
}
```

### Run

Android:

    react-native run-android

iOS:

    react-native run-ios

## License

[GPL-3.0](LICENSE)