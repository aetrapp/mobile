# Contribute

## Introduction

Thanks for considering contributing to AeTrapp!

We welcome any type of contribution, not only code. You can help with:
- **QA**: file bug reports, the more details you can give the better;
- **Community**: presenting the project at meetups, organizing a dedicated meetup for the local community;
- **Code**: take a look at the [open issues](https://github.com/aetrapp/mobile/issues). Even if you can't write code, commenting on them, showing that you care about a given issue matters. It helps us triage them.

## Submitting code

Any code change should be submitted as a pull request. The description should explain what the code does and give steps to execute it. The pull request should also contain tests, if applicable.

## Code review process

The bigger the pull request, the longer it will take to review and merge. Try to break down large pull requests in smaller chunks that are easier to review and merge.

It is also always helpful to have some context for your pull request. What was the purpose? Why does it matter to you?

## Questions

If you have any questions, create an [issue](https://github.com/aetrapp/mobile/issues) (protip: do a quick search first to see if someone else didn't ask the same question before!).

You can also reach us at suporte@aetrapp.org.

<!-- This `CONTRIBUTING.md` is based on @nayafia's template https://github.com/nayafia/contributing-template -->
