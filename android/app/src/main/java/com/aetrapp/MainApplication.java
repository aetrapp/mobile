package com.aetrapp;

import android.support.annotation.Nullable;

import com.facebook.react.ReactPackage;
import com.reactnativenavigation.NavigationApplication;
import org.reactnative.camera.RNCameraPackage;
import com.rnfs.RNFSPackage;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.horcrux.svg.SvgPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;

import java.util.List;
import java.util.Arrays;

public class MainApplication extends NavigationApplication {

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages(){
    return Arrays.<ReactPackage>asList(
      new RNCameraPackage(),
      new RNFSPackage(),
      new ImagePickerPackage(),
      new VectorIconsPackage(),
      new MapsPackage(),
      new ReactVideoPackage(),
      new SvgPackage(),
      new ReactNativeOneSignalPackage()
    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }


}
