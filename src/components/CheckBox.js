import React, { Component } from 'react';

// Theme
import getTheme from 'native-base-theme/components';
import material from 'native-base-theme/variables/aetrapp';

// Native base
import { CheckBox as NBCheckBox, StyleProvider } from 'native-base';

export default class CheckBox extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <NBCheckBox {...this.props}>
          {this.props.children}
        </NBCheckBox>
      </StyleProvider>
    );
  }
}
