import React from "react";
import { StyleSheet } from "react-native";
import { Button } from "react-native-elements";
import { colors } from "styles";

class StyledButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const type = this.props.type || "primary";
    let options = Object.assign(
      { style: styles[type] },
      buttonOptions[type],
      this.props
    );
    options.title = options.title.toUpperCase();

    return <Button {...options}>{this.props.children}</Button>;
  }
}

const buttonOptions = {
  primary: {
    backgroundColor: colors.apple,
    color: "#fff",
    uppercase: true,
    marginBottom: 20
  }
};

const styles = StyleSheet.create({
  primary: {
    marginBottom: 20
  }
});

export default StyledButton;
