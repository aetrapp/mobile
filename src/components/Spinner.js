import React from "react";
import SpinnerOverlay from "react-native-loading-spinner-overlay";

import { colors } from "styles";

class Spinner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { color, overlayColor, text } = colors.spinner;
    return <SpinnerOverlay
      {...this.props}
      color={color}
      overlayColor={overlayColor}
      textContent={"Aguarde"}
      textStyle={{
        color: text,
        fontFamily: 'Century Gothic'
      }}
    />
  }
}

export default Spinner;
