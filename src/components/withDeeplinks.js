import React from "react";

// App state
import { AppState } from "react-native";

// Helper to keep static non-react related methods
import hoistNonReactStatic from 'hoist-non-react-statics';

// Handle deep links from nofitications
import { Navigation } from "react-native-navigation";
import OneSignal from 'react-native-onesignal';

// Navigation options
import navigationOptions from "screens/navigationOptions";

OneSignal.addEventListener('opened', action => {
  Navigation.handleDeepLink({
    link: 'notification',
    payload: action.notification
  });
});
OneSignal.inFocusDisplaying(2);

function withDeeplinks(WrappedComponent) {
  class DeepLinkHandler extends React.Component {
    constructor(props) {
      super(props);

      // Navigation
      this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

      // Bindings
      this._handleAppStateChange = this._handleAppStateChange.bind(this);
      this.openNotification = this.openNotification.bind(this);

      this.state = {
        appState: AppState.currentState,
        pendingNotificationOpen: false
      };
    }

    componentDidMount() {
      AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
      AppState.removeEventListener('change', this._handleAppStateChange);
    }

    openNotification(notificationId) {

      // Close any opened screens
      this.props.navigator.popToRoot();

      // Change to notifications tab
      this.props.navigator.switchToTab({ tabIndex: 2 });

      // Push notification screen
      this.props.navigator.push({
        ...navigationOptions("aetrapp.Notifications.Show"),
        passProps: {
          id: notificationId
        }
      });
    }

    _handleAppStateChange = (nextAppState) => {
      // Watch when application return from inactivity
      if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {

        // If pending notification to be opened
        if (this.state.pendingNotificationOpen) {

          // Fire notification open
          this.openNotification(this.state.pendingNotificationOpen);

          // Clear notification
          this.setState({ pendingNotificationOpen: false });
        }
      }
      this.setState({ appState: nextAppState });
    }

    onNavigatorEvent(event) {
      // Register nofitication to be opened when app become "active";
      if (event.type == "DeepLink") {

        const { notificationId } = event.payload.payload.additionalData;

        this.setState({
          pendingNotificationOpen: notificationId
        });
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  // Preserve static methods
  hoistNonReactStatic(DeepLinkHandler, WrappedComponent);

  return DeepLinkHandler;
}

export default withDeeplinks;