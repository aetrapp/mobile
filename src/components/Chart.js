import _ from "lodash";


// React & Redux
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dimensions } from "react-native";

// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Style
import { colors } from "styles";

// Componenents
import {
  VictoryArea,
  VictoryAxis,
  VictoryChart,
  VictoryLine,
  VictoryLabel,
  VictoryLegend,
  VictoryScatter
} from "victory-native";

class Chart extends React.Component {
  static navigatorStyle = {
    navBarHidden: true
  };

  constructor(props) {
    super(props);

    // Bindings
    this.getStyles = this.getStyles.bind(this);
    this.getTrapDataset = this.getTrapDataset.bind(this);

    // Prepare data
    const dateFormat = "DD/MM";
    const domainInterval = 35;
    const domainDays = _.times(domainInterval, i => {
      return moment().subtract(domainInterval - i - 1, 'days');
    });
    const trapDataset = this.getTrapDataset(domainDays);

    // Set state
    this.state = {
      domainInterval,
      domainDays,
      trapDataset,
      dateFormat
    }
  }

  render() {
    const { domainInterval, domainDays, trapDataset, dateFormat } = this.state;
    const styles = this.getStyles();
    const cityDataset = this.getCityDataset();

    let tickValues = [];
    for (let index = domainInterval; index >= 0; index--) {
      tickValues.push(moment().subtract(index, 'days').toDate().setHours(0, 0, 0, 0));
    }

    const height = 300;
    const { width } = Dimensions.get("window");
    const padding = { left: 50, right: 50, top: 50, bottom: 70 };
    const domain = [moment().subtract(domainInterval, 'days').toDate().setHours(0, 0, 0, 0), new Date().setHours(0, 0, 0, 0)];

    if (trapDataset.length == 0) return null
    else
      return <VictoryChart
        padding={padding}
      >

        <VictoryLabel x={width / 2} y={24} style={styles.title}
          text="Histórico de Contagens"
        />

        <VictoryAxis
          scale="time"
          domain={[moment().subtract(domainInterval, 'days').toDate().setHours(0, 0, 0, 0), new Date().setHours(0, 0, 0, 0)]}
          style={styles.daysAxis}
          tickValues={tickValues}
          tickFormat={(x, i) => {
            const date = moment(x);
            if (i % 7 === 0)
              return date.format(dateFormat);
          }}
        />

        <VictoryAxis dependentAxis
          style={styles.countAxis}
          tickCount={4}
        />


        {cityDataset && cityDataset.length ? (
          <VictoryArea
            data={cityDataset}
            style={styles.cityCount}
            domain={{ x: [moment().subtract(domainInterval, 'days').toDate().setHours(0, 0, 0, 0), new Date()] }}
            x='collectedAt'
            y='eggCount'
          />
        ) : null}

        {
          trapDataset && trapDataset.length > 1 &&
          <VictoryLine
            data={trapDataset}
            style={styles.trapCount.line}
            domain={{ x: domain }}
            x='collectedAt'
            y='eggCount'
          />
        }

        <VictoryScatter
          data={trapDataset}
          style={styles.trapCount.points}
          domain={{ x: domain }}
          x='collectedAt'
          y='eggCount'
        />

        <VictoryLegend
          gutter={10}
          y={height - padding.bottom / 2}
          x={(width - 295) / 2}
          orientation="horizontal"
          style={styles.legend}
          data={[
            {
              name: "Quantidade de ovos",
              symbol: {
                type: "square",
                fill: styles.trapCount.points.data.fill,
              }
            },
            {
              name: "Média municipal",
              symbol: {
                type: "square",
                stroke: styles.cityCount.data.stroke,
                fill: styles.cityCount.data.fill,
                fillOpacity: styles.cityCount.data.fillOpacity,
              },
            }
          ]}
        />
      </VictoryChart>

  }

  getCityDataset() {
    const { domainDays } = this.state;
    const { eggCountAverages } = this.props.city;
    return _.compact(_.map(domainDays, day => {
      const weekString = moment(day).format("YYYY-w");
      if(eggCountAverages[weekString]) {
        return {
          collectedAt: day.toDate().setHours(0, 0, 0, 0),
          eggCount: eggCountAverages[weekString] || null
        }
      }
    }));
  }

  getTrapDataset(domainDays) {
    const { samples } = this.props;
    let dataset = {};

    // discard older counts
    const domainStart = domainDays[0].toISOString();
    let recentCounts = _.filter(samples, i => {
      return (i.eggCount !== null && (i.collectedAt > domainStart))
    });

    // make hash map from dates
    recentCounts.forEach(item => {
      dataset[moment(item.collectedAt).startOf('day').format("DD/MM/YYYY")] = item;
    });

    // make array again
    dataset = _.map(dataset, i => {
      return {
        ...i,
        collectedAt: moment(i.collectedAt).startOf('day').toDate()
      };
    });

    return dataset;
  }

  getStyles() {
    const { domainInterval } = this.state;

    return {
      chart: {
        padding: 100
      },
      title: {
        textAnchor: "middle",
        fontWeight: "bold"
      },
      legend: {
      },
      daysAxis: {
        axis: { stroke: "black", strokeWidth: 1 },
        ticks: {
          size: (tick, i) => {
            // every seven days tick is bigger 
            const date = moment(tick);
            return (i % 7 === 0) ? 8 : 5;
          },
          stroke: "black",
          strokeWidth: 1
        },
        tickLabels: {
          fill: "black",
          fontFamily: "inherit"
        }
      },
      countAxis: {
        grid: {
          stroke: (tick) =>
            tick === 0 ? "transparent" : 'rgba(0, 0, 0, 0.4)',
          strokeWidth: 1,
          strokeDasharray: '6, 6',
        },
        ticks: {
          size: 5,
          stroke: 'black',
        },
      },
      trapCount: {
        line: {
          data: {
            stroke: colors.apple
          }
        },
        points: {
          data: {
            width: 10,
            fill: colors.apple
          }
        }
      },
      cityCount: {
        data: {
          fill: colors.sunglow,
          stroke: colors.sunglow,
          strokeWidth: 2,
          fillOpacity: 0.3
        }
      }
    }
  }

}

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Chart);
