import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native-elements";

class StyledHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let options = Object.assign(
      { style: styles.header },
      this.props
    );
    return <Text {...options}>{this.props.children}</Text>;
  }
}

var styles = StyleSheet.create({
  header: {
    fontFamily: "Simplifica",
    padding: 15
  }
});

export default StyledHeader;
