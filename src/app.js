// React Native Navigation
import { Navigation } from "react-native-navigation";
import { registerScreens, registerScreenVisibilityListener } from "./screens";

// Actions
import { signIn } from "./actions/auth";
import { initCities } from "./actions/cities";

// Helpers
import loadIcons from "./helpers/loadIcons";
import initWatchPosition from "helpers/init-watch-position";

// Styles
import { colors } from "styles";

// Redux & State Management
import { Provider } from "react-redux";
import configureStore from "./store";
import initServices from "./services";

const store = configureStore(store => {
  initServices(store, () => {
    const state = store.getState();
    if(state.auth.accessToken) {
      store.dispatch(signIn());
    }
    store.dispatch(initCities());
    initWatchPosition(store.dispatch);
  });
});

registerScreens(store, Provider);
// registerScreenVisibilityListener();

export default class App {

  constructor() {
    store.subscribe(this.onStoreUpdate.bind(this));
    loadIcons()
      .then(icons => {
        this.icons = icons;
      })
      .catch(error => {
        console.log("Error loading icons", error); // eslint-disable-line
      });
  }

  onStoreUpdate() {
    const { signedIn } = store.getState().auth;
    if (this.currentSignInStatus != signedIn) {
      this.currentSignInStatus = signedIn;

      if (signedIn) {
        Navigation.startTabBasedApp({
          tabs: [
            {
              label: "Mapa",
              screen: "aetrapp.Traps.Map",
              icon: this.icons.map
            },
            {
              label: "Armadilhas",
              screen: "aetrapp.Traps",
              icon: this.icons.home
            },
            {
              label: "Notificações",
              screen: "aetrapp.Notifications.Index",
              icon: this.icons.bars
            },
            {
              label: "Ajuda",
              screen: "aetrapp.Help.Index",
              icon: this.icons.help
            },
            {
              label: "Colaborador",
              screen: "aetrapp.Profile",
              title: "Colaborador",
              icon: this.icons.user
            }
          ],
          appStyle: {
            hideBackButtonTitle: true,
            tabFontFamily: 'Century Gothic', // < - Begining of Android tab style 
            tabBarBackgroundColor: colors.tundora,
            tabBarButtonColor: '#fff',
            tabBarSelectedButtonColor: colors.apple,
            tabBarTranslucent: false
          },
          tabsStyle: {
            tabFontFamily: 'Century Gothic', // < - Begining of iOS tab style 
            tabBarBackgroundColor: colors.tundora,
            tabBarButtonColor: '#fff',
            tabBarSelectedButtonColor: colors.apple,
            tabBarTranslucent: false
          }
        });
      } else {
        Navigation.startSingleScreenApp({
          screen: {
            screen: "aetrapp.Welcome"
          },
          navigatorStyle: {
            navBarHidden: true,
          }
        });
      }
    }
  }
}
