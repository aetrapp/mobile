import { connect } from "react-redux";
import React from "react";

// Actions
import { signIn } from "actions/auth";

// Service
import client from "services/helpers/client";
const authManagement = client.service("authManagement");

// Components & Styles
import { Button, CheckBox } from "components";
import { Alert, Keyboard } from "react-native";
import { Body, Container, Label, Form, ListItem, Input, Item, Text } from "native-base";

// Validators
import { validateEmail, validatePassword } from "helpers/validators";

// Helper
import showErrorAlert from "helpers/error-alert";

class SignIn extends React.Component {

  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.props.navigator.setButtons({
      rightButtons: [{
        title: 'Esqueci a senha',
        id: 'next'
      }]
    });

    // Bindings
    this.toggleSecureTextEntry = this.toggleSecureTextEntry.bind(this);
    this.sendCredentials = this.sendCredentials.bind(this);
    this.resetPasswordRequest = this.resetPasswordRequest.bind(this);
    this.sendVerification = this.sendVerification.bind(this);

    // State
    this.state = {
      requesting: false,
      showPassword: false,
      selection: { start: 0, end: 0 },
      values: {}
    };
  }

  componentDidUpdate(prevProps, prevState) {
    // Hide navbar when requesting
    if (this.state.requesting !== prevState.requesting) {
      if (this.state.requesting == true) {
        this.props.navigator.toggleNavBar({to: 'hidden'});
      } else {
        this.props.navigator.toggleNavBar({to: 'shown'});
      }
    }
  }

  sendCredentials() {
    const { email, password } = this.state.values;

    // Validate e-mail
    const emailResult = validateEmail(email);
    if (!emailResult.isValid()) {
      Alert.alert('Campo Inválido.', emailResult.firstError().message);
      return;
    }

    // Validate password
    const passwordResult = validatePassword(password);
    if (!passwordResult.isValid()) {
      Alert.alert('Campo Inválido.', passwordResult.firstError().message);
      return;
    }

    // Hide keyboard
    Keyboard.dismiss();

    // Make request
    this.setState({ requesting: true });
    this.props.signIn({ email, password })
      .catch(err => {
        if (err.code == 401)
          Alert.alert('Login inválido', 'Verifique se e-mail e senha estão corretos.');
        else
          showErrorAlert(err);
      })
      .finally(()=>{
        this.setState({ requesting: false });
      });
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      this.resetPasswordRequest();
    }
  }

  resetPasswordRequest() {    
    const { email } = this.state.values;    

    const emailResult = validateEmail(email);
    if (!emailResult.isValid()) {
      Alert.alert('Campo Inválido.', emailResult.firstError().message);
      return false;
    }
    
    this.setState({ requesting: true });
    authManagement
      .create({
        action: 'sendResetPwd',
        value: { email }
      })
      .then(() => {
        Alert.alert("Recuperação de senha", "Um link de recuperação de senha foi enviado ao seu email.", [
          { text: 'OK', onPress: () => this.props.navigator.pop() },
        ], { cancelable: false });
        this.setState({ requesting: false });
      })
      .catch(err => {
        if (err.errors && err.errors.$className == "isVerified") {
          this.sendVerification();
        } else {
          Alert.alert("Erro", err.message);
          this.setState({ requesting: false });
        }
      });
  }

  sendVerification() {
    const { email } = this.state;
    authManagement
      .create({
        action: 'resendVerifySignup',
        value: { email }
      })
      .then(() => {
        Alert.alert("Aviso", "É preciso confirmar seu endereço de e-mail para recuperar a senha. Verifique sua caixa de entrada.", [
          { text: 'OK', onPress: () => this.props.navigator.pop() },
        ], { cancelable: false });
      })
      .catch(err => {
        Alert.alert("Erro", err.message);
      })
      .finally(() => {
        this.setState({ requesting: false });
      });
  }

  toggleSecureTextEntry() {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }

  onChange(fieldName, text) {
    var newState = { ...this.state };
    newState.values[fieldName] = text;
    this.setState(newState);
  }

  componentDidMount() {
    this.emailInput._root.focus();
  }

  render() {
    const { email, password } = this.state.values;
    return (
      <Container>
        <Form>
          <Item>
            <Label>E-mail</Label>
            <Input
              ref={ref => { this.emailInput = ref; }}
              value={email}
              keyboardType='email-address'
              autoCapitalize='none'
              returnKeyType='next'
              onChangeText={text => this.onChange("email", text)}
              onSubmitEditing={() => { this.passwordInput._root.focus(); }}
            />
          </Item>
          <Item>
            <Label>Senha</Label>
            <Input
              ref={ref => { this.passwordInput = ref; }}
              value={password}
              secureTextEntry={!this.state.showPassword}
              autoCapitalize={"none"}
              onChangeText={text => this.onChange("password", text)}
              onSubmitEditing={() => { this.sendCredentials(); }}
            />
          </Item>
          <ListItem onPress={() => this.toggleSecureTextEntry()}>
            <CheckBox
              checked={this.state.showPassword}
              onPress={() => this.toggleSecureTextEntry()}
            />
            <Body>
              <Text>Exibir senha</Text>
            </Body>
          </ListItem>
          <Button 
            disabled={this.state.requesting} 
            title={this.state.requesting ? "Enviando..." : "Enviar" }
            onPress={this.sendCredentials} />
        </Form>
      </Container>
    );
  }
}

const mapDispatchToProps = {
  signIn
};

export default connect(null, mapDispatchToProps)(SignIn);