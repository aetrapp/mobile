import { connect } from "react-redux";
import React from "react";
import t from "tcomb-validation";

// Components & Styles
import { Alert } from "react-native";
import {
  Container,
  Form,
  Label,
  Input,
  Item
} from "native-base";
import { Spinner } from "components";

// Actions
import { signUp } from "actions/auth";

// Helper
import showErrorAlert from "helpers/error-alert";

class SetContact extends React.Component {
  constructor(props) {
    super(props);

    // initial state
    this.state = {
      loading: false,
      values: {}
    };

    // Navigator
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // bindings
    this.onChange = this.onChange.bind(this);
    this.submit = this.submit.bind(this);

  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      this.submit();
    }
  }

  onChange(fieldName, text) {
    var newState = { ...this.state };
    newState.values[fieldName] = text;
    this.setState(newState);
  }

  validate() {
    const { password, confirmPassword } = this.state.values;

    const PasswordString = t.refinement(t.String, function (s) {
      return s && s.length >= 6 && s.length < 50;
    });

    if (!(t.validate(password, PasswordString).isValid())) {
      Alert.alert('Digite novamente', 'Senha deve ter no mínimo 6 caracteres.');
    } else if (password != confirmPassword) {
      Alert.alert('Digite novamente', 'A confirmação de senha não coincide.');
    } else {
      return true;
    }

    return false;
  }

  submit() {
    if (this.validate()) {
      this.setState({ loading: true });
      this.props.signUp(Object.assign({}, this.props.userData, this.state.values))
        .then(()=>{
          this.setState({ loading: false });
          setTimeout(()=>{
            Alert.alert('Usuário registrado!', 'Cadastro feito com sucesso na plataforma, acione o botão "Entrar" para fazer o login na plataforma.', [
              { text: 'OK', onPress: () => this.props.navigator.popToRoot() },
            ], { cancelable: false });
          }, 10);
        })
        .catch(err => {
          this.setState({ loading: false });
          setTimeout(()=>{
            if (err.errors && err.errors[0] && err.errors[0].message &&  err.errors[0].message == 'email must be unique') {
              Alert.alert('Erro ao registrar novo usuário:', 'Já existe usuário cadastrado com este e-mail');
            } else {
              showErrorAlert(err);
            }
          }, 10);
        });
    }
  }

  render() {
    const { password, confirmPassword } = this.state.values;
    return (
      <Container>
        <Spinner visible={this.state.loading} />
        <Form>
          <Item>
            <Label>Senha</Label>
            <Input
              autoFocus
              ref='passwordInput'
              value={password}
              secureTextEntry
              autoCapitalize='none'
              returnKeyType='next'
              onSubmitEditing={() => { this.refs.repeatPasswordInput._root.focus(); }}
              onChangeText={text => this.onChange("password", text)}
            />
          </Item>
          <Item>
            <Label>Confirme a senha:</Label>
            <Input
              ref='repeatPasswordInput'
              value={confirmPassword}
              secureTextEntry
              autoCapitalize='none'
              returnKeyType='go'
              onSubmitEditing={this.submit}
              onChangeText={text => this.onChange("confirmPassword", text)}
            />
          </Item>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.signUp
  };
};

const mapDispatchToProps = {
  signUp
};

export default connect(mapStateToProps, mapDispatchToProps)(SetContact);
