import { connect } from "react-redux";
import React from "react";

// Navigation
import navigationOptions from "screens/navigationOptions";

// Components & Styles
import { Alert } from "react-native";
import {
  Container,
  Content,
  Form,
  Label,
  Input,
  Item,
  Picker
} from "native-base";
import { TextInputMask } from 'react-native-masked-text';

// Actions
import { updateSignUpData } from "actions/auth/signUp";

// Validators
import {
  validateEmail,
  validateFirstName,
  validateLastName,
  validateCellphone,
  validateBrDate
} from "helpers/validators";


class SetUserInfo extends React.Component {
  constructor(props) {
    super(props);

    // Navigator
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onChangeField = this.onChangeField.bind(this);
    this.submit = this.submit.bind(this);

    // State
    this.state = {
      values: {
        gender: 'undisclosed',
      }
    };
  }

  componentDidMount() {
    this.refs.emailInput._root.focus();
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {

      // Refresh values from TextMaskInput components . On Android, "onBlur" do not 
      // fire this event ("next") when input is focused.
      const cellphoneNumberInput = this.refs['cellphoneNumberInput'].getElement()._root;
      if (cellphoneNumberInput.isFocused()) {
        cellphoneNumberInput.blur();
        this.onChangeField('cellphoneNumber', this.refs['cellphoneNumberInput'].getRawValue().toString());
      }

      const landlineNumberInput = this.refs['landlineNumberInput'].getElement()._root;
      if (landlineNumberInput.isFocused()) {
        landlineNumberInput.blur();
        this.onChangeField('landlineNumber', this.refs['landlineNumberInput'].getRawValue().toString());
      }

      const dateOfBirthInputValue = this.refs['dateOfBirthInput'].getRawValue();
      this.onChangeField('dateOfBirth', dateOfBirthInputValue.isValid() ? dateOfBirthInputValue.format('DD/MM/YYYY') : null);

      this.submit();
    }
  }

  submit() {
    const {
      email,
      firstName,
      lastName,
      cellphoneNumber,
      dateOfBirth
    } = this.state.values;

    const emailResult = validateEmail(email);
    if (!emailResult.isValid()) {
      Alert.alert('Preenchimento incorreto', emailResult.firstError().message);
      return;
    }

    const firstNameResult = validateFirstName(firstName);
    if (!firstNameResult.isValid()) {
      Alert.alert('Preenchimento incorreto', firstNameResult.firstError().message);
      return;
    }

    const lastNameResult = validateLastName(lastName);
    if (!lastNameResult.isValid()) {
      Alert.alert('Preenchimento incorreto', lastNameResult.firstError().message);
      return;
    }

    const cellphoneResult = validateCellphone(cellphoneNumber);
    if (!cellphoneResult.isValid()) {
      Alert.alert('Preenchimento incorreto', cellphoneResult.firstError().message);
      return;
    }

    const dateofBirthResult = validateBrDate(dateOfBirth);
    if (!dateofBirthResult.isValid()) {
      Alert.alert('Preenchimento incorreto', dateofBirthResult.firstError().message);
      return;
    }

    this.props.updateSignUpData(this.state.values);
    this.props.navigator.push(navigationOptions("aetrapp.Auth.SignUp.Step2"));
  }

  onChangeField(fieldName, value) {
    let state = { ...this.state };
    state.values[fieldName] = value;
    this.setState(state);
  }

  render() {
    const {
      dateOfBirth,
      cellphoneNumber,
      landlineNumber,
      email,
      firstName,
      gender,
      lastName
    } = this.state.values;

    return (
      <Container>
        <Content>
          <Form>
            <Item>
              <Label>E-mail</Label>
              <Input
                value={email}
                ref='emailInput'
                keyboardType='email-address'
                autoCapitalize='none'
                returnKeyType='next'
                onChangeText={text => this.onChangeField("email", text)}
                onSubmitEditing={() => { this.refs.firstNameInput._root.focus(); }}
              />
            </Item>
            <Item>
              <Label>Nome</Label>
              <Input
                value={firstName}
                ref='firstNameInput'
                autoCapitalize='words'
                returnKeyType='next'
                onSubmitEditing={() => { this.refs.lastNameInput._root.focus(); }}
                onChangeText={text => this.onChangeField('firstName', text)}
              />
            </Item>
            <Item>
              <Label>Sobrenome</Label>
              <Input
                value={lastName}
                ref='lastNameInput'
                autoCapitalize='words'
                returnKeyType='next'
                onSubmitEditing={() => { this.refs['cellphoneNumberInput'].getElement()._root.focus(); }}
                onChangeText={text => this.onChangeField('lastName', text)}
              />
            </Item>
            <Item>
              <Label>Celular</Label>
              <TextInputMask
                ref='cellphoneNumberInput'
                value={cellphoneNumber}
                type={'cel-phone'}
                customTextInput={Input}
                returnKeyType='next'
                onSubmitEditing={() => { 
                  this.refs['landlineNumberInput'].getElement()._root.focus(); 
                }}
                onBlur={() => {
                  this.onChangeField('cellphoneNumber', this.refs['cellphoneNumberInput'].getRawValue().toString());
                }}
              />
            </Item>
            <Item>
              <Label>Telefone fixo</Label>
              <TextInputMask
                ref="landlineNumberInput"
                value={landlineNumber}
                type={'cel-phone'}
                customTextInput={Input}
                returnKeyType='next'
                onSubmitEditing={() => { this.refs['dateOfBirthInput'].getElement()._root.focus(); }}
                onBlur={() => {
                  this.onChangeField('landlineNumber', this.refs['landlineNumberInput'].getRawValue().toString());
                }}
              />
            </Item>
            <Item>
              <Label>Nascimento</Label>
              <TextInputMask
                ref='dateOfBirthInput'
                type={'datetime'}
                options={{
                  format: 'DD/MM/YYYY'
                }}
                value={dateOfBirth}
                customTextInput={Input}
                onBlur={() => {
                  const inputValue = this.refs['dateOfBirthInput'].getRawValue();
                  this.onChangeField('dateOfBirth', inputValue.isValid() ? inputValue.format('DD/MM/YYYY') : null);
                }}
                returnKeyType='next'
              />
            </Item>
            <Item>
              <Label>Gênero</Label>
              <Picker
                ref='genderPicker'
                mode="dropdown"
                style={{ flex: 1 }}
                selectedValue={gender}
                onValueChange={value => { this.onChangeField('gender', value); }}
              >
                <Picker.Item label="Feminino" value="female" />
                <Picker.Item label="Masculino" value="male" />
                <Picker.Item label="Outro" value="other" />
                <Picker.Item label="Não informado" value="undisclosed" />
              </Picker>
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = {
  updateSignUpData
};

export default connect(null, mapDispatchToProps)(SetUserInfo);
