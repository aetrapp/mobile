import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Button, StyleSheet } from "react-native";
import { signOut } from "../../actions/auth";

class SignOut extends React.Component {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    this.props.signOut();
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="SignOut" onPress={this.onPress} />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    padding: 20
  }
});

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = {
  signOut
};

export default connect(mapStateToProps, mapDispatchToProps)(SignOut);
