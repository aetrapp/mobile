import React, { Component } from "react";
import { connect } from "react-redux";

// Localized moment
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Navigation
import navigationOptions from "screens/navigationOptions";

// Components
import { Alert, View } from "react-native";
import { Spinner } from "components";
import {
  Body,
  Content,
  Icon,
  H3,
  ListItem,
  List,
  Right,
  Text,
} from "native-base";

// Actions
import { initSampleAssistant } from "actions/sampleAssistant";
import { patchTrap } from "actions/traps";
import { findSamples } from "actions/samples";
import { findCities } from "actions/cities";

// Helpers
import { setTrapStatusAndColor } from "helpers/traps";

class ShowTrap extends Component {
  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.showPicture = this.showPicture.bind(this);
    this.showSampleAssistant = this.showSampleAssistant.bind(this);
    this.requestSampleAssistant = this.requestSampleAssistant.bind(this);
    this.showSampleList = this.showSampleList.bind(this);
    this.initNextCycle = this.initNextCycle.bind(this);
    this.reactivateTrap = this.reactivateTrap.bind(this);
    this.deactivateTrap = this.deactivateTrap.bind(this);

    // State
    this.state = {
      spinnerVisible: false
    };
  }

  onNavigatorEvent(event) {
    if (event.id == "willAppear") {
      const { trap, user } = this.props;

      if (trap.status != 'inactive' && trap.ownerId == user.id) {
        if (trap.cycleIsFinished) {
          Alert.alert(
            'Amostra analisada com sucesso!', 
            'Uma amostra válida foi recebida e agora é preciso iniciar novo ciclo ou desativar a armadilha, escolhendo uma opção na seção "Ações".');
        } else if ((trap.age - trap.cycleDuration + 1) > 0) {
          Alert.alert(
            'Ciclo em atraso!', 
            'Envie uma amostra válida, inicie um novo ciclo ou desative a armadilha.');
        }
      }

      this.props.findSamples({
        trapId: trap.id,
        $sort: {
          collectedAt: -1
        }
      });
      this.props.findCities({
        id: trap.cityId
      });
    }
  }

  showPicture() {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.ShowPicture"),
      passProps: {
        blobId: this.props.trap.imageId
      }
    });
  }

  showSampleAssistant() {
    this.props.initSampleAssistant({
      trapId: this.props.trap.id
    });
    this.props.navigator.push(navigationOptions("aetrapp.Samples.Assistant.SelectSource"));
  }

  requestSampleAssistant() {
    if (this.props.trap.daysToCollect < 1) this.showSampleAssistant();
    else
      Alert.alert(
        'A amostra não está pronta',
        'Deseja realmente enviar amostra fora da janela de coleta?',
        [
          { text: 'Cancelar', style: 'cancel' },
          {
            text: 'OK', onPress: () => {
              this.showSampleAssistant();
            }
          },
        ]
      );
  }

  showSampleList() {
    this.props.navigator.push({
      screen: "aetrapp.Samples.List",
      title: "Amostras",
      passProps: {
        sampleIds: this.props.trap.samples,
        trapId: this.props.trap.id
      }
    });
  }

  initNextCycle() {
    Alert.alert(
      'Iniciar próximo ciclo:',
      'A sua armadilha está pronta para um novo período de coleta?',
      [
        { text: 'Não', style: 'cancel' },
        {
          text: 'Sim', onPress: () => {
            this.setState({ spinnerVisible: true });
            this.props.patchTrap(this.props.trap.id, {
              restartCycle: true
            }).then(() => {
              this.props.navigator.pop();
            }).catch(err => {
              Alert.alert('Erro', err.message);
            }).finally(() => {
              this.setState({ spinnerVisible: false });
            });
          }
        },
      ]
    );
  }


  reactivateTrap() {
    Alert.alert(
      'Reativar armadilha?',
      'Um novo ciclo de coleta será iniciado agora, verifique se sua armadilha está pronta.',
      [
        { text: 'Cancelar', style: 'cancel' },
        {
          text: 'OK', onPress: () => {
            this.setState({ spinnerVisible: true });
            this.props.patchTrap(this.props.trap.id, {
              status: 'active'
            }).then(() => {
              this.props.navigator.pop();
            }).catch(err => {
              Alert.alert('Erro', err.message);
            }).finally(() => {
              this.setState({ spinnerVisible: false });
            });
          }
        },
      ]
    );
  }

  deactivateTrap() {
    Alert.alert(
      'Desativar armadilha?',
      'Ao executar esta ação a sua armadilha irá para a lista de arquivadas, mas poderá ser reativada no futuro.',
      [
        { text: 'Cancelar', style: 'cancel' },
        {
          text: 'OK', onPress: () => {
            this.setState({ spinnerVisible: true });

            this.props.patchTrap(this.props.trap.id, {
              status: 'inactive'
            }).then(() => {
              this.props.navigator.pop();
            }).catch(err => {
              Alert.alert('Erro', err.message);
            }).finally(() => {
              this.setState({ spinnerVisible: false });
            });
          }
        },
      ]
    );
  }

  shouldComponentUpdate(nextProps) {
    // avoid update when sample is removed
    return (typeof nextProps.trap !== "undefined");
  }

  render() {
    const { trap, user } = this.props;
    const { eggCountSeries, city } = trap;

    return (
      <Content>
        <List>
          <Spinner visible={this.state.spinnerVisible} />
          <ListItem itemDivider></ListItem>
          <ListItem icon>
            <Body>
              <Text>Identificador</Text>
            </Body>
            <Right>
              <Text selectable>{trap.id}</Text>
            </Right>
          </ListItem>
          <ListItem icon>
            <Body>
              <Text>Situação</Text>
            </Body>
            <Right>
              <Text>{trap.statusMessage}</Text>
            </Right>
          </ListItem>
          {city && city.currentMeanCount >= 0 && trap.meanStatus &&
            <View>
              <ListItem icon>
                <Body>
                  <Text>Média da cidade</Text>
                </Body>
                <Right>
                  <Text>{city.currentMeanCount} ovos</Text>
                </Right>
              </ListItem>
              <ListItem icon>
                <Body>
                  <Text>Relação à média</Text>
                </Body>
                <Right>
                  <Text>{trap.meanStatus}</Text>
                </Right>
              </ListItem>
            </View>
          }
          <ListItem itemDivider>
            <H3>Local</H3>
          </ListItem>
          <ListItem>
            <Body>
              <Text note>
                {trap.addressStreet}
              </Text>
              {!!trap.addressComplement &&
                <Text note >
                  {trap.addressComplement}
                </Text>
              }
              {!!trap.addressPostcode &&
                <Text note>
                  CEP {trap.addressPostcode}
                </Text>
              }
              {!!trap.neighbourhood &&
                <Text note>
                  {trap.neighbourhood}
                </Text>
              }
              {!!trap.city &&
                <Text note>
                  {trap.city.name}, {trap.stateId}
                </Text>
              }
            </Body> 
          </ListItem>
          {
            eggCountSeries && eggCountSeries.length > 0 &&
            <View>
              <ListItem itemDivider>
                <H3>Última amostra</H3>
              </ListItem>
              <ListItem icon>
                <Body>
                  <Text>Contagem</Text>
                </Body>
                <Right>
                  <Text>{eggCountSeries[0].eggCount} ovos</Text>
                </Right>
              </ListItem>
              <ListItem icon>
                <Body>
                  <Text>Data da coleta</Text>
                </Body>
                <Right>
                  <Text>{moment(eggCountSeries[0].collectedAt).format("DD/MM/YYYY HH:mm")}</Text>
                </Right>
              </ListItem>

            </View>
          }
          <ListItem itemDivider>
            <H3>Ações</H3>
          </ListItem>
          {
            (trap.ownerId == user.id) && (trap.cycleIsFinished || trap.daysToCollect < 1) && trap.status != "inactive" &&
            <ListItem icon button onPress={this.initNextCycle}>
              <Body>
                <Text>Iniciar próximo ciclo</Text>
              </Body>
              <Right>
                <Icon name="refresh" />
              </Right>
            </ListItem>
          }
          {
            trap.ownerId == user.id && trap.status != "inactive" &&
            <ListItem icon button onPress={this.requestSampleAssistant}>
              <Body>
                <Text>Fotografar amostra</Text>
              </Body>
              <Right>
                <Text>
                  {trap.daysToCollect < 1 ? "Hoje" : trap.windowStart}
                </Text>
                <Icon name="camera" />
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          }
          {
            trap.sampleCount > 0 &&
            <View>
              <ListItem icon button onPress={this.showSampleList}>
                <Body>
                  <Text>Ver amostras enviadas</Text>
                </Body>
                <Right>
                  <Text>{trap.sampleCount}</Text>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>
            </View>
          }
          {
            trap.imageId &&
            <View>
              <ListItem icon button onPress={this.showPicture}>
                <Body>
                  <Text>Ver imagem da armadilha</Text>
                </Body>
                <Right>
                  <Icon name="image" />
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>
            </View>
          }
          {trap.ownerId == user.id && (
            trap.status != 'inactive' ?
              <ListItem icon button onPress={this.deactivateTrap}>
                <Body>
                  <Text>Desativar armadilha</Text>
                </Body>
                <Right>
                  <Icon name="arrow-round-down" />
                </Right>
              </ListItem>
              :
              <ListItem icon button onPress={this.reactivateTrap}>
                <Body>
                  <Text>Reativar armadilha</Text>
                </Body>
                <Right>
                  <Icon name="arrow-round-up" />
                </Right>
              </ListItem>
          )
          }
        </List>
      </Content>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let trap = state.traps[ownProps.trapId];
  if (!trap) return {};
  trap = setTrapStatusAndColor(trap);
  return { trap, user: state.auth.user };
};


const mapDispatchToProps = {
  initSampleAssistant,
  patchTrap,
  findSamples,
  findCities
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowTrap);
