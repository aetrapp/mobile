import { connect } from "react-redux";
import React from "react";

// Components
import navigationOptions from "screens/navigationOptions";
import { Body, Container, Content, Icon, ListItem, Right, Text } from 'native-base';
import ImagePicker from "react-native-image-picker";
import { Spinner } from "components";

// Actions
import { updateCreateAssistant } from "actions/traps/assistant";

class SelectSource extends React.Component {
  constructor(props) {
    super(props);

    // Bindings
    this.captureFromCamera = this.captureFromCamera.bind(this);
    this.selectFromGalery = this.selectFromGalery.bind(this);

    // State
    this.state = {
      spinnerVisible: false
    };
  }

  captureFromCamera() {
    this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.TakePicture"));
  }

  selectFromGalery() {
    this.setState({ spinnerVisible: true });
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      this.setState({ spinnerVisible: false });
      if (!response.didCancel && !response.error) {
        this.props.updateCreateAssistant({
          filePath: "file://" + response.path
        });

        this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.ReviewPicture"));
      }
    });
  }

  render() {
    return (
      <Container>
        <Content>
          <Spinner visible={this.state.spinnerVisible} />
          <ListItem itemDivider>
            <Text>Escolha a fonte de envio:</Text>
          </ListItem>
          <ListItem button onPress={this.captureFromCamera} >
            <Body>
              <Text>Câmera do celular</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem button onPress={this.selectFromGalery} >
            <Body>
              <Text>Galeria de fotos</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  updateCreateAssistant
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectSource);
