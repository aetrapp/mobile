import { connect } from "react-redux";
import React from "react";

// Navigation
import navigationOptions from "screens/navigationOptions";

// Components
import { View, Dimensions } from "react-native";
import Image from "react-native-transformable-image-next";
import { Spinner } from "components";

class ReviewPicture extends React.Component {
  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onLayout = this.onLayout.bind(this);

    // State
    const { width, height } = Dimensions.get("window");
    this.state = {
      width,
      height
    };
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.Location"));
    }
  }

  onLayout() {
    const { width, height } = Dimensions.get("window");
    this.setState({
      width,
      height
    });
  }

  render() {
    const { filePath } = this.props.trap;
    const { width, height } = this.state;

    return (
      <View style={{ backgroundColor: "black" }} onLayout={this.onLayout}>
        <Spinner visible={this.state.spinnerVisible}></Spinner>
        <Image
          style={{ width: width, height: height }}
          source={{ uri: filePath }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    trap: state.trapCreateAssistant
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewPicture);
