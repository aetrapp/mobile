import _ from "lodash";
import React from "react";
import { connect } from "react-redux";

// Style
import { colors } from "styles";

// Navigation
import navigationOptions from "screens/navigationOptions";

// Components
import {
  Alert,
  Dimensions,
  Image,
  StyleSheet,
  View,
} from "react-native";
import { Icon, Fab } from "native-base";

// Map
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// HTTP Client
import axios from "axios";
const httpClient = axios.create();

// Collections
import { states } from "collections/states.json";
import { cities } from "collections/cities.json";

// Actions
import { updateCreateAssistant } from "actions/traps/assistant";


class SetTrapLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bidings    
    this._onRegionChange = this._onRegionChange.bind(this);
    this.getAddress = this.getAddress.bind(this);
    this.panToUserlocation = this.panToUserlocation.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      this.getAddress();
      this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.City"));
    }
  }

  _onRegionChange(region) {
    this.props.updateCreateAssistant({
      coordinates: {
        type: "Point",
        coordinates: [region.longitude, region.latitude],
        crs: {
          type: "name",
          properties: {
            name: "EPSG:4326"
          }
        }
      }
    });
  }

  getAddress() {
    const self = this;

    if (!this.props.data.coordinates) return;

    const query = {
      format: "json",
      "accept-language": "pt",
      addressdetails: 1,
      zoom: 18,
      lat: this.props.data.coordinates.coordinates[1],
      lon: this.props.data.coordinates.coordinates[0],
      email: "contato@aparabolica.com.br"
    };

    httpClient
      .get("https://nominatim.openstreetmap.org/reverse", {
        params: query
      })
      .then(function (res) {
        // Get address parts
        const { state, city, suburb, road } = res.data.address;

        // Parse city/state id
        const stateId = _.find(states, ["name", state]).id;
        const cityId = _.find(cities, {
          stateId,
          name: city
        });

        self.props.updateCreateAssistant({
          stateId: stateId,
          cityId: cityId && cityId.id,
          neighbourhood: suburb,
          addressStreet: road
        });
      })
      .catch(function (err) {
        /* eslint-disable no-console */
        console.log('error fetching address');
        console.log(err);
        /* eslint-enable no-console */
      });
  }

  panToUserlocation() {
    const { location } = this.props;

    if (!location.isAvailable) {
      Alert.alert("Localização indisponível", "Habilite a localização do celular para centralizar o mapa ao seu local.");
    } else {
      this.map.animateToRegion({
        latitude: location.lat,
        longitude: location.lon,
        latitudeDelta: 0.0005,
        longitudeDelta: 0.0005 * ASPECT_RATIO
      });
    }
  }

  render() {
    const { isAvailable, lat, lon } = this.props.location;
    const defaultMapRegion = {
      latitude: -13.972713555694922,
      latitudeDelta: 63.29989498713658,
      longitude: -50.158063620328896,
      longitudeDelta: 48.63221291452646
    };

    if (!this.mapRegion) {
      this.mapRegion = isAvailable ? {
        longitude: lon,
        latitude: lat,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      } : defaultMapRegion;
    }

    return (
      <View style={styles.container}>

        <MapView
          ref={ref => { this.map = ref; }}
          style={styles.map}
          onRegionChangeComplete={this._onRegionChange}
          provider={PROVIDER_GOOGLE}
          initialRegion={this.mapRegion}
        />
        <View style={styles.center} />
        <Image
          style={styles.centerMarker}
          source={require("../../../../images/marker.png")}
        />
        <Fab
          style={{ backgroundColor: colors.apple }}
          onPress={this.panToUserlocation}
        >
          <Icon name="locate" />
        </Fab>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  center: {
    position: "absolute",
    top: "50%",
    left: "50%",
    width: 20,
    height: 20,
    marginLeft: -10,
    marginTop: -10,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: "#fff",
    overflow: "visible"
  },
  centerMarker: {
    width: 50,
    height: 46,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -46
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  select: {
    margin: 20
  }
});

const mapStateToProps = (state) => {
  return {
    data: state.trapCreateAssistant,
    location: state.location
  };
};

const mapDispatchToProps = {
  updateCreateAssistant
};

export default connect(mapStateToProps, mapDispatchToProps)(SetTrapLocation);
