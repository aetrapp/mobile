import { connect } from "react-redux";
import React from "react";

// Navigation 
import navigationOptions from "screens/navigationOptions";

// Components
import {
  Alert,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import { Spinner } from "components";
import { RNCamera } from "react-native-camera";
import Icon from "react-native-vector-icons/FontAwesome";

// Actions
import { updateCreateAssistant } from "actions/traps/assistant";

// Styles
var styles = StyleSheet.create({
  portrait: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  landscape: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  preview: {
    ...StyleSheet.absoluteFillObject
  }
});

class TakePicture extends React.Component {
  constructor(props) {
    super(props);

    // Bindings
    this.takePicture = this.takePicture.bind(this);

    // State
    this.state = {
      spinnerVisible: false,
      orientation: this.getOrientation()
    };
  }

  getOrientation() {
    const { width, height } = Dimensions.get("window");
    return width < height ? "portrait" : "landscape";
  }

  onLayout = () => {
    this.setState({ orientation: this.getOrientation() });
  };

  takePicture = async function () {
    this.setState({ spinnerVisible: true });
    if (this.camera) {
      try {
        const data = await this.camera.takePictureAsync({
          fixOrientation: true
        });
        this.props.updateCreateAssistant({ filePath: data.uri });
        this.setState({ spinnerVisible: false });
        this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.ReviewPicture"));
      } catch (error) {
        this.setState({ spinnerVisible: false });
        Alert.alert('Erro', 'Não foi possível registrar a imagem.');
      }
    }
  }

  render() {
    const { orientation } = this.state;
    return (
      <View style={styles[orientation]} onLayout={this.onLayout}>
        <Spinner visible={this.state.spinnerVisible} />
        <RNCamera
          ref={cam => {
            this.camera = cam;
          }}
          style={styles.preview}
          permissionDialogTitle={'Permissão para usar a câmera'}
          permissionDialogMessage={'Por favor, autorize o uso da camêra pelo AeTrapp'}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', margin: 20 }}>
          <TouchableOpacity onPress={this.takePicture}>
            <Icon name="camera" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  updateCreateAssistant
};

export default connect(mapStateToProps, mapDispatchToProps)(TakePicture);
