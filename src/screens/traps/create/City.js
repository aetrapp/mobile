import _ from "lodash";
import { connect } from "react-redux";
import React from "react";

// Styles
import formStyles from "styles/form";

// Navigation
import navigationOptions from "screens/navigationOptions";

// Components
import { View, StyleSheet } from "react-native";
import { Content } from "native-base";
import { Spinner } from "components";

// Collections
import { states } from "collections/states.json";
import { cities } from "collections/cities.json";

// Form
import t from "tcomb-form-native"; // eslint-disable-line import/default
const { Form } = t.form;
const statesEnum = {};
_.each(states, state => {
  statesEnum[state.id] = state.name;
});
var States = t.enums(statesEnum);
var Cities = t.enums({});

const baseType = {
  stateId: States,
  cityId: Cities,
  neighbourhood: t.String,
  postcode: t.maybe(t.String)
};

const options = {
  stylesheet: formStyles,
  fields: {
    stateId: {
      label: "Estado",
      isCollapsed: true
    },
    cityId: {
      label: "Cidade",
      isCollapsed: true
    },
    neighbourhood: {
      label: "Bairro"
    },
    postcode: {
      label: "CEP"
    }
  }
};

// Actions
import { updateCreateAssistant } from "actions/traps/assistant";

class City extends React.Component {
  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onChange = this.onChange.bind(this);
    this.getType = this.getType.bind(this);
    this.getCitiesEnum = this.getCitiesEnum.bind(this);

    // State
    const value = this.props.data;
    this.state = {
      spinnerVisible: false,
      value,
      type: this.getType(value)
    };
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      var value = this.refs.form.getValue();
      if (value) {
        this.props.updateCreateAssistant(value);
        this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.Address"));
      }
    }
  }

  getCitiesEnum(state) {
    let citiesEnum = {};
    if (state) {
      let selectedCities = _.filter(cities, city => {
        return city.stateId == state;
      });
      _.each(selectedCities, city => {
        citiesEnum[city.id] = city.name;
      });
    }
    return citiesEnum;
  }

  getType(value) {
    const { stateId } = value;
    if (stateId) {
      return t.struct(
        Object.assign(
          {},
          baseType,
          {
            cityId: t.enums(this.getCitiesEnum(stateId))
          }
        )
      );
    } else {
      return t.struct(baseType);
    }
  }

  onChange(value) {
    if (value.stateId != this.state.value.stateId) {
      delete value.cityId;
      this.setState({
        value,
        type: this.getType(value)
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const trap = nextProps.data;
    const value = Object.assign(
      {},
      this.state.value,
      {
        stateId: trap.stateId,
        cityId: trap.cityId,
        neighbourhood: trap.neighbourhood,
        postcode: trap.postcode
      }
    );
    this.setState({
      value,
      type: this.getType(value)
    });
  }

  render() {
    const { value, type } = this.state;

    return (
      <View style={styles.container}>
        <Spinner visible={this.state.spinnerVisible} />

        <Content>
          <Form
            ref="form"
            type={type}
            value={value}
            options={options}
            onChange={this.onChange}
          />
        </Content>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = (state) => {
  return {
    data: state.trapCreateAssistant
  };
};

const mapDispatchToProps = {
  updateCreateAssistant
};

export default connect(mapStateToProps, mapDispatchToProps)(City);
