// React and Redux
import { connect } from "react-redux";
import React from "react";

// Modules
import RNFS from "react-native-fs";

// Styles
import formStyles from "styles/form";

// Components
import { Alert, View, StyleSheet } from "react-native";
import { Content } from "native-base";
import { Spinner } from "components";

// Form
import t from "tcomb-form-native"; // eslint-disable-line import/default
const { Form } = t.form;

const baseType = {
  addressStreet: t.String,
  addressComplement: t.maybe(t.String),
};

const options = {
  stylesheet: formStyles,
  fields: {
    addressStreet: {
      label: "Endereço"
    },
    addressComplement: {
      label: "Complemento"
    },
  }
};

// Service
import client from "services/helpers/client";
const traps = client.service("traps");
traps.timeout = 60000;

// Actions
import { createTrap } from "actions/traps";
import { updateCreateAssistant } from "actions/traps/assistant";


class GetAddress extends React.Component {
  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onChange = this.onChange.bind(this);
    this.getType = this.getType.bind(this);

    // State
    const value = this.props.data;
    this.state = {
      spinnerVisible: false,
      value,
      type: this.getType(value)
    };
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      var value = this.refs.form.getValue();
      if (value) {
        this.props.updateCreateAssistant(value);
        this.setState({ spinnerVisible: true });

        const { filePath } = this.props.data;

        // Read File
        RNFS.readFile(filePath, "base64")
          .then(contents => {
            // Submit
            this.props
              .createTrap({
                ...this.props.data,
                base64: `data:image/jpeg;base64,${contents}`
              })
              .then(() => {
                this.props.navigator.popToRoot();
              })
              .catch(err => {
                Alert.alert('Erro', err.message);
                console.log("error posting trap image", err); // eslint-disable-line no-console
              })
              .finally(() => {
                this.setState({ spinnerVisible: false });
              });
          })
          .catch(err => {
            Alert.alert('Erro', err.message);
          });

      }
    }
  }

  getType() {
    return t.struct(baseType);
  }

  onChange(value) {
    if (value.stateId != this.state.value.stateId) {
      this.setState({
        value,
        type: this.getType(value)
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const trap = nextProps.data;
    const value = Object.assign(
      this.state.value,
      {
        addressStreet: trap.addressStreet,
        addressComplement: trap.addressComplement,
      }
    );
    this.setState({ value, type: this.getType(value) });
  }

  render() {
    const { value, type } = this.state;

    return (
      <View style={styles.container}>
        <Spinner visible={this.state.spinnerVisible} />

        <Content>
          <Form
            ref="form"
            type={type}
            value={value}
            options={options}
            onChange={this.onChange}
          />
        </Content>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = (state) => {
  return {
    data: state.trapCreateAssistant
  };
};

const mapDispatchToProps = {
  updateCreateAssistant,
  createTrap
};

export default connect(mapStateToProps, mapDispatchToProps)(GetAddress);
