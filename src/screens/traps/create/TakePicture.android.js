import { connect } from "react-redux";
import React from "react";

// Navigation 
import navigationOptions from "screens/navigationOptions";

// Components
import {
  Alert,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import { Spinner } from "components";
import Camera from "react-native-camera";
import Icon from "react-native-vector-icons/FontAwesome";

// Actions
import { updateCreateAssistant } from "actions/traps/assistant";

class TakePicture extends React.Component {
  constructor(props) {
    super(props);

    // Bindings
    this.takePicture = this.takePicture.bind(this);

    // State
    this.state = {
      spinnerVisible: false,
      orientation: this.getOrientation()
    };
  }

  getOrientation() {
    const { width, height } = Dimensions.get("window");
    return width < height ? "portrait" : "landscape";
  }

  onLayout = () => {
    this.setState({ orientation: this.getOrientation() });
  };

  takePicture() {
    const options = {};
    this.setState({
      spinnerVisible: true
    });
    return this.camera
      .capture({ metadata: options })
      .then(data => {
        this.setState({
          spinnerVisible: false
        });
        this.props.updateCreateAssistant({
          filePath: data.path
        });
        this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.ReviewPicture"));
      })
      .catch(err => {
        this.setState({
          spinnerVisible: false
        });
        if (err.code == "failed to save image file") {
          Alert.alert('Erro', "Habilite a permissão de acesso ao armazenamento nas configurações do aplicativo.");
        } else {
          Alert.alert('Erro', "Não foi possível salvar a imagem.");
        }
      });
  }

  render() {
    const { orientation } = this.state;
    return (
      <View style={styles[orientation]} onLayout={this.onLayout}>
        <Spinner visible={this.state.spinnerVisible} />

        <Camera
          ref={cam => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}
        />
        <View style={styles.snap}>
          <TouchableOpacity onPress={this.takePicture}>
            <Icon name="camera" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  portrait: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  landscape: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  preview: {
    ...StyleSheet.absoluteFillObject
  },
  snap: {
    margin: 30
  }
});

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  updateCreateAssistant
};

export default connect(mapStateToProps, mapDispatchToProps)(TakePicture);
