import React from 'react';
import { connect } from 'react-redux';

// Navigator
import navigationOptions from "screens/navigationOptions";

// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Colors
import { colors } from "styles";

import {
  Alert,
  Dimensions,
  View,
  StyleSheet,
} from "react-native";

import {
  Text,
  Button,
  Fab,
  Icon
} from "native-base";

// Actions
import { findTraps } from "actions/traps";

// Helpers
import loadIcons from "helpers/loadIcons";

// Map
import MapView, { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
const { width, height } = Dimensions.get('window');
const LATITUDE_DELTA = 65;
const ASPECT_RATIO = width / height;

// Style
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  zoomIn: {
    position: 'absolute',
    right: 10,
    bottom: (height / 2)
  },
  zoomOut: {
    position: 'absolute',
    right: 10,
    top: (height / 2)
  },
  locateButton: {
    backgroundColor: colors.apple,
  },
  optionsButon: {
    backgroundColor: colors.apple,
    bottom: 66
  },
  tooltipText: {
    fontSize: 10
  }
});

class TrapsMap extends React.Component {
  static navigatorStyle = {
    navBarHidden: true
  };

  constructor(props) {
    super(props);

    // Bindings
    this._enterTrap = this._enterTrap.bind(this);
    this.panToUserlocation = this.panToUserlocation.bind(this);
    this.openMapOptions = this.openMapOptions.bind(this);
    this.onPressZoomIn = this.onPressZoomIn.bind(this);
    this.onPressZoomOut = this.onPressZoomOut.bind(this);
    this.centerMap = this.centerMap.bind(this);

    // Initial state
    const { location } = props;
    const initialRegion = location && location.isAvailable ?
      {
        latitude: location.lat,
        longitude: location.lon,
        latitudeDelta: 0.0005,
        longitudeDelta: 0.0005 * ASPECT_RATIO
      } : {
        latitude: -11.918027712921148,
        longitude: -54.905702322721474,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO
      };
    this.state = {
      mapIsLoaded: false,
      showOptions: false,
      initialRegion,
      currentRegion: initialRegion
    };

    // Marker icons
    loadIcons()
      .then(icons => {
        this.icons = icons;
      })
      .catch(error => {
        console.log("Error loading icons", error); // eslint-disable-line
      });
  }

  componentDidMount() {
    this.props.findTraps({
      $limit: 50,
      $sort: {
        createdAt: -1
      }
    });
  }

  _enterTrap(trapId) {
    return (e) => {
      e.preventDefault();
      this.props.navigator.push({
        ...navigationOptions("aetrapp.Traps.Show"),
        passProps: {
          trapId: trapId
        }
      });
    };
  }

  panToUserlocation() {
    const { location } = this.props;

    if (!location.isAvailable) {
      Alert.alert("Localização indisponível", "Habilite a permissão de localização nas configurações do aplicativo.");
    } else {
      this.map.animateToRegion({
        latitude: location.lat,
        longitude: location.lon,
        latitudeDelta: 0.0005,
        longitudeDelta: 0.0005 * ASPECT_RATIO
      });
    }
  }

  onPressZoomIn() {
    const newRegion = {
      latitude: this.state.currentRegion.latitude,
      longitude: this.state.currentRegion.longitude,
      latitudeDelta: this.state.currentRegion.latitudeDelta / 5,
      longitudeDelta: this.state.currentRegion.longitudeDelta / 5
    };
    this.setState({ currentRegion: newRegion });
    this.map.animateToRegion(newRegion, 400);
  }

  onPressZoomOut() {
    const newRegion = {
      latitude: this.state.currentRegion.latitude,
      longitude: this.state.currentRegion.longitude,
      latitudeDelta: this.state.currentRegion.latitudeDelta * 5,
      longitudeDelta: this.state.currentRegion.longitudeDelta * 5
    };
    this.setState({ currentRegion: newRegion });
    this.map.animateToRegion(newRegion, 400);
  }

  centerMap(event) {
    const { coordinate } = event.nativeEvent;
    if (coordinate) {
      const newRegion = {
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
        latitudeDelta: this.state.currentRegion.latitudeDelta,
        longitudeDelta: this.state.currentRegion.longitudeDelta
      };
      this.setState({ currentRegion: newRegion });
      this.map.animateToRegion(newRegion, 400);
    }
  }

  onMapRegionChangeComplete(region) {
    this.setState({
      currentRegion: region
    });
  }

  openMapOptions() {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Traps.Map.Options")
    });
  }

  render() {
    const { traps, satelliteBaseLayer } = this.props;

    // show map only after icons are loaded
    return this.icons ? <View style={styles.container}>
      <MapView
        ref={ref => { this.map = ref; }}
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        mapType={satelliteBaseLayer ? 'satellite' : 'standard'}
        initialRegion={this.state.initialRegion}
        rotateEnabled={false}
        showsMyLocationButton={false}
        toolbarEnabled={false}
        moveOnMarkerPress={false}
        onRegionChangeComplete={region => { this.setState({ currentRegion: region }); }}
      >
        {traps && traps.map(trap => (
          <Marker
            key={trap.id}
            image={this.icons && this.icons[trap.status]}
            toolbarEnabled={false}
            onPress={this.centerMap}
            coordinate={{
              latitude: trap.coordinates.coordinates[1],
              longitude: trap.coordinates.coordinates[0]
            }}
          >
            <Callout onPress={this._enterTrap(trap.id)}>
              <View style={{ margin: 10, width: 150 }}>
                <Text style={[styles.tooltipText, { fontWeight: 'bold', fontSize: 20 }]} >{trap.addressStreet}</Text >
                <Text style={styles.tooltipText}>ID {trap.id}</Text>
                <Text style={styles.tooltipText}>{trap.statusMessage}</Text>
                <Text style={styles.tooltipText}>{trap.sampleCount} amostras enviadas</Text>
                {typeof (trap.eggCount) != 'undefined' && trap.eggCount != null &&
                  <Text style={styles.tooltipText}>{trap.eggCount} ovos em {moment(trap.eggCountDate).format("DD/MM/YYYY")}</Text>
                }
                <Text style={[styles.tooltipText, { fontStyle: 'italic', marginTop: 10 }]}>Ver mais detalhes...</Text >
              </View>
            </Callout>
          </Marker>
        ))}
      </MapView>
      <Fab
        onPress={this.openMapOptions}
        style={styles.optionsButon}
      >
        <Icon name="settings" />
      </Fab>
      <Fab
        style={styles.locateButton}
        onPress={this.panToUserlocation}
      >
        <Icon name="locate" />
      </Fab>
      <Button style={styles.zoomIn} light onPress={this.onPressZoomIn}>
        <Icon name='add' />
      </Button>
      <Button style={styles.zoomOut} light onPress={this.onPressZoomOut}>
        <Icon name='remove' />
      </Button>
    </View >
      : null;
  }
}

const mapStateToProps = (state) => {

  // Transform traps into array
  let traps = Object.keys(state.traps).map(id => state.traps[id]);

  // Apply map filter
  const mapFilterOnStatuses = state.context.mapFilterOnStatuses || {
    "waiting-sample": true,
    "no-eggs": true,
    "above-average": true,
    "below-average": true,
    "delayed": true,
    "inactive": false
  };
  traps = traps.filter(trap => {
    return mapFilterOnStatuses[trap.status];
  });

  return {
    satelliteBaseLayer: state.context.satelliteBaseLayer,
    traps,
    location: state.location
  };
};

const mapDispatchToProps = {
  findTraps
};

export default connect(mapStateToProps, mapDispatchToProps)(TrapsMap);
