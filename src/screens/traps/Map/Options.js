import { connect } from "react-redux";
import React from "react";

// Components
import { CheckBox } from "components";
import {
  Container,
  Content,
  Body,
  ListItem,
  Text,
  H2
} from "native-base";

// Actions
import { updateContext } from "actions/context";

class MapOptions extends React.Component {
  constructor(props) {
    super(props);
  }

  toggleStatusFilter(status) {
    let mapFilterOnStatuses = this.props.mapFilterOnStatuses;
    mapFilterOnStatuses[status] = !mapFilterOnStatuses[status];
    this.props.updateContext("mapFilterOnStatuses", mapFilterOnStatuses);
  }

  toggleBaseLayer(layerName) {
    this.props.updateContext("satelliteBaseLayer", layerName == 'satellite');
  }

  render() {
    const { mapFilterOnStatuses, satelliteBaseLayer } = this.props;

    return (
      <Container>
        <Content>
          <ListItem itemDivider>
            <H2>Camada base:</H2>
          </ListItem>    
          <ListItem onPress={() => this.toggleBaseLayer('map')}>
            <CheckBox checked={!satelliteBaseLayer} onPress={() => this.toggleBaseLayer('map')}/>
            <Body>
              <Text>Mapa</Text>
            </Body>
          </ListItem>                
          <ListItem onPress={() => this.toggleBaseLayer('satellite')}>
            <CheckBox checked={satelliteBaseLayer} onPress={() => this.toggleBaseLayer('satellite')}/>
            <Body>
              <Text>Satélite</Text>
            </Body>
          </ListItem>                
          <ListItem itemDivider>
            <H2>Exibir armadilhas:</H2>
          </ListItem>
          <ListItem onPress={() => this.toggleStatusFilter('delayed')}>
            <CheckBox checked={mapFilterOnStatuses['delayed']} onPress={() => this.toggleStatusFilter('delayed')} />
            <Body>
              <Text>Atrasadas</Text>
            </Body>
          </ListItem>
          <ListItem onPress={() => this.toggleStatusFilter('above-average')}>
            <CheckBox checked={mapFilterOnStatuses['above-average']} onPress={() => this.toggleStatusFilter('above-average')} />
            <Body>
              <Text>Acima da média</Text>
            </Body>
          </ListItem>
          <ListItem onPress={() => this.toggleStatusFilter('below-average')}>
            <CheckBox checked={mapFilterOnStatuses['below-average']} onPress={() => this.toggleStatusFilter('below-average')} />
            <Body>
              <Text>Abaixo da média</Text>
            </Body>
          </ListItem>
          <ListItem onPress={() => this.toggleStatusFilter('no-eggs')}>
            <CheckBox checked={mapFilterOnStatuses['no-eggs']} onPress={() => this.toggleStatusFilter('no-eggs')} />
            <Body>
              <Text>Sem ovos</Text>
            </Body>
          </ListItem>
          <ListItem onPress={() => this.toggleStatusFilter('waiting-sample')}>
            <CheckBox checked={mapFilterOnStatuses['waiting-sample']} onPress={() => this.toggleStatusFilter('waiting-sample')} />
            <Body>
              <Text>Aguardando amostra</Text>
            </Body>
          </ListItem>
          <ListItem onPress={() => this.toggleStatusFilter('inactive')}>
            <CheckBox checked={mapFilterOnStatuses['inactive']} onPress={() => this.toggleStatusFilter('inactive')} />
            <Body>
              <Text>Desativadas</Text>
            </Body>
          </ListItem>
        </Content>
      </Container>
    );
  }
}


const mapStateToProps = state => {
  return {
    satelliteBaseLayer: state.context.satelliteBaseLayer || false,
    mapFilterOnStatuses: state.context.mapFilterOnStatuses || {
      "waiting-sample": true,
      "no-eggs": true,
      "above-average": true,
      "below-average": true,
      "delayed": true,
      "inactive": false
    }
  };
};

const mapDispatchToProps = {
  updateContext
};

export default connect(mapStateToProps, mapDispatchToProps)(MapOptions);
