import _ from "lodash";
import React from "react";
import { connect } from "react-redux";

// Permissions
import checkPermissions from "helpers/check-permissions";

// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Navigator
import { navigatorStyle } from "styles";
import navigationOptions from "screens/navigationOptions";

// Components
import { View } from "react-native";
import { Body, Content, Icon, List, ListItem, Text, Right } from "native-base";

// Actions
import { findTraps } from "actions/traps";
import { initCreateTrapAssistant } from "actions/traps/assistant";

class Traps extends React.Component {
  static navigatorStyle = navigatorStyle;

  constructor(props) {
    super(props);

    this.props.navigator.setTitle({ title: "Suas armadilhas" });

    // Bindings
    this.showTrap = this.showTrap.bind(this);
    this.createTrap = this.createTrap.bind(this);
  }

  componentDidMount() {
    checkPermissions();
    this.props.findTraps({ ownerId: this.props.userId });
  }

  createTrap() {
    this.props.initCreateTrapAssistant();
    this.props.navigator.push(navigationOptions("aetrapp.Traps.Create.PictureSource"));
  }

  showTrap(trap) {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Traps.Show"),
      passProps: {
        trapId: trap.id
      }
    });
  }

  render() {
    const { inactiveTraps, activeTraps } = this.props;
    return (
      <Content>

        <ListItem itemDivider></ListItem>
        <ListItem button onPress={this.createTrap}>
          <Body>
            <Text>Iniciar armadilha em novo endereço</Text>
          </Body>
          <Right>
            <Icon name="add" />
          </Right>
        </ListItem>
        {activeTraps.length > 0 &&
          <View>
            <ListItem itemDivider><Text>Armadilhas Ativas</Text></ListItem>
            {activeTraps.map(trap => (
              <ListItem key={trap.id} button onPress={() => { this.showTrap(trap); }}>
                <Body>
                  <Text>{trap.addressStreet}</Text>
                  <Text note>{trap.city && trap.city.name}, {trap.stateId}</Text>
                  <Text note>Coletas: {trap.sampleCount}</Text>
                  <Text note>Criada em {moment(trap.createdAt).format("DD/MM/YY HH:mm")}</Text>
                </Body>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>
            ))
            }
          </View>
        }
        {
          inactiveTraps.length > 0 &&
          <View>
            <ListItem itemDivider>
              <Text>Armadilhas Desativadas</Text>
            </ListItem>
            <List>
              {inactiveTraps.map(trap => (
                <ListItem key={trap.id} button onPress={() => { this.showTrap(trap); }}>
                  <Body>
                    <Text>{trap.addressStreet}</Text>
                    <Text note>{trap.city && trap.city.name}, {trap.stateId}</Text>
                    <Text note>Coletas: {trap.sampleCount}</Text>
                    <Text note>Criada em {moment(trap.createdAt).format("DD/MM/YY HH:mm")}</Text>
                  </Body>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
              ))}
            </List>
          </View>
        }
      </Content>
    );
  }
}

const mapStateToProps = (state) => {
  const userId = state.auth.user.id;
  const cities = state.cities;

  let ownTraps = _.filter(state.traps, { "ownerId": userId });
  ownTraps.map(trap => {
    trap.city = cities[trap.cityId];
    return trap;
  });
  ownTraps = _.castArray(ownTraps);
  ownTraps.sort((a, b) => a.updatedAt < b.updatedAt);

  const activeTraps = _.filter(ownTraps, trap => trap.status != 'inactive');
  const inactiveTraps = _.filter(ownTraps, { status: 'inactive' });

  return {
    userId: state.auth.user.id,
    activeTraps,
    inactiveTraps
  };
};

const mapDispatchToProps = {
  initCreateTrapAssistant,
  findTraps
};

export default connect(mapStateToProps, mapDispatchToProps)(Traps);
