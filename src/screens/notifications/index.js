import _ from "lodash";
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Navigator
import { navigatorStyle } from "styles";
import navigationOptions from "screens/navigationOptions";

// Components
import { Spinner } from "components";
import { Alert, View, ListView } from "react-native";
import { Body, Button, Content, Icon, List, ListItem, Right, Text } from "native-base";

// Actions
import { findNotifications, removeNotification } from "actions/notifications";

// Deep Links
import withDeeplinks from "components/withDeeplinks";

class Notifications extends React.Component {
  static navigatorStyle = navigatorStyle;

  constructor(props) {
    super(props);

    this.props.navigator.setTitle({ title: "Suas notificações" });

    // Bindings
    this.openTarget = this.openTarget.bind(this);
    this.removeNotification = this.removeNotification.bind(this);

    // State
    this.state = {
      spinnerVisible: false
    };
  }

  componentDidMount() {
    // Load own notifications
    this.props.findNotifications({
      $sort: {
        createdAt: -1,
        deliveryTime: -1
      },
      $and: [{
        recipientId: this.props.user.id,
        deliveryTime: {
          $or: [
            { $lte: Date.now() },
            { $eq: null }
          ]
        },
      }]
    });
  }

  openTarget(notification) {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Notifications.Show"),
      passProps: {
        id: notification.id
      }
    });
  }

  removeNotification(id) {
    this.setState({ spinnerVisible: true });
    this.props.removeNotification(id)
      .catch(() => {
        Alert.alert('Não foi possível remover a notificação.', 'Tente novamente mais tarde.');
      })
      .finally(() => {
        this.setState({ spinnerVisible: false });
      });
  }

  render() {
    let { notifications } = this.props;
    const { removeNotification } = this;

    // List DataSource
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1.id !== r2.id });

    return (
      <Content>
        <Spinner visible={this.state.spinnerVisible} />

        {
          notifications.length > 0 ?
            <View>
              <ListItem itemDivider>
                <Text note>
                  Deslize para as laterais para remover
                </Text>
              </ListItem>

              <List
                dataSource={ds.cloneWithRows(notifications)}
                renderRow={notification =>
                  <ListItem onPress={() =>
                    this.props.navigator.push({
                      ...navigationOptions("aetrapp.Notifications.Show"),
                      passProps: {
                        id: notification.id
                      }
                    })              
                  }>
                    <Body>
                      <Text note>{moment(notification.deliveryTime || notification.createdAt).format("DD/MM/YYYY HH:mm")}</Text>
                      <Text>{notification.title}</Text>
                    </Body>
                    <Right>
                      <Icon name="arrow-forward" />
                    </Right>
                  </ListItem>}
                renderLeftHiddenRow={(notification, secId, rowId, rowMap) =>
                  <Button danger onPress={() => {
                    rowMap[`${secId}${rowId}`].props.closeRow();
                    removeNotification(notification.id);
                  }}>
                    <Icon active name="trash" />
                  </Button>}
                renderRightHiddenRow={(notification, secId, rowId, rowMap) =>
                  <Button danger onPress={() => {
                    rowMap[`${secId}${rowId}`].props.closeRow();
                    removeNotification(notification.id);
                  }}>
                    <Icon active name="trash" />
                  </Button>
                }
                leftOpenValue={75}
                rightOpenValue={-75}
              />
            </View>
            :
            <ListItem itemDivider>
              <Text>Não há notificações</Text>
            </ListItem>
        }
      </Content>
    );
  }
}

const mapStateToProps = (state) => {
  const user = state.auth.user;

  // filter notifications to display here
  let notifications = _.filter(state.notifications, notification => {

    // reject all future notifications
    const deliveryTime = new Date(notification.deliveryTime);
    const now = new Date();
    if (deliveryTime.getTime() > (now.getTime() + 60000)) {
      return false;
    }

    // accept all directed to the user
    if (notification.recipientId == user.id) return true;

    // accept all global
    if (notification.type == 'global') return true;

    return false;
  });

  notifications = _.castArray(notifications);
  notifications = _.orderBy(notifications, a => {
    return new Date(a.deliveryTime || a.createdAt);
  }, 'desc');

  return {
    user: state.auth.user,
    notifications: notifications
  };
};

const mapDispatchToProps = {
  findNotifications,
  removeNotification
};

/* 
  This screen is mounted with Navigator.startTabBasedApp() and will stay mounted until the 
  user sign out. It is assumed it will always be able to listen to "DeepLink" events, so 
  the High Order Component "withDeeplinks" is used here.    
*/
export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withDeeplinks
)(Notifications);
