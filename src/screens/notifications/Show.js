import React, { Component } from "react";
import { connect } from "react-redux";

// Navigator
import navigationOptions from "screens/navigationOptions";

// Style
import { colors } from "styles";

// Components
import { Spinner } from "components";
import { Alert, StyleSheet, View } from "react-native";
import {
  Button,
  Container,
  Content,
  H1,
  Text,
} from "native-base";

// Actions
import {
  getNotification,
  removeNotification
} from "actions/notifications";

var styles = StyleSheet.create({
  content: {
    padding: 20
  },
  item: {
    marginBottom: 10
  },
  buttom: {
    backgroundColor: colors.apple
  }
});

class ShowNotification extends Component {
  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.loadNotification = this.loadNotification.bind(this);
    this.removeNotification = this.removeNotification.bind(this);

    // State
    this.state = {
      spinnerVisible: false
    };
  }

  onNavigatorEvent(event) {
    if (event.id == "willAppear") {
      if (!this.props.notification)
        this.loadNotification();
    }
  }

  shouldComponentUpdate(nextProps) {
    // avoid update when sample is removed
    return (typeof nextProps.notification !== "undefined");
  }

  showTrap(trapId) {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Traps.Show"),
      passProps: {
        trapId
      }
    });
  }

  showSample(sampleId) {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Samples.Show"),
      passProps: {
        sampleId
      }
    });
  }

  loadNotification() {
    const { id } = this.props;
    this.setState({ spinnerVisible: true });
    this.props.getNotification(id)
      .catch(() => {
        Alert.alert('Não foi possível carregar a notificação.', 'Tente novamente mais tarde.');
        this.props.navigator.pop();
      })
      .finally(() => {
        this.setState({ spinnerVisible: false });
      });

  }

  removeNotification() {
    const { id } = this.props;
    this.setState({ spinnerVisible: true });
    this.props.removeNotification(id)
      .then(() => {
        this.props.navigator.pop();
      })
      .catch(() => {
        Alert.alert('Não foi possível remover a notificação.', 'Tente novamente mais tarde.');
      })
      .finally(() => {
        this.setState({ spinnerVisible: false });
      });
  }

  render() {
    const { notification } = this.props;

    return (
      <Container >
        <Content style={styles.content}>
        <Spinner visible={this.state.spinnerVisible} />
          {
            notification &&
            <View>
              <H1 style={styles.item}>
                {notification.title}
              </H1>
              <Text style={styles.item}>
                {notification.message}
              </Text>
              {
                notification.payload.trapId &&
                <Button block rounded={false} style={[styles.item, styles.buttom]}
                  onPress={() => this.showTrap(notification.payload.trapId)}>
                  <Text>Exibir armadilha</Text>
                </Button>
              }
              {
                notification.payload.sampleId &&
                <Button block rounded={false} style={[styles.item, styles.buttom]}
                  onPress={() => this.showSample(notification.payload.sampleId)}>
                  <Text>Exibir amostra</Text>
                </Button>
              }
              <Button block rounded={false} danger style={styles.item}
                onPress={() => this.removeNotification()}>
                <Text>Descartar</Text>
              </Button>
            </View>
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const notification = state.notifications[ownProps.id];

  if (!notification) return {};

  return { notification };
};


const mapDispatchToProps = {
  getNotification,
  removeNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowNotification);
