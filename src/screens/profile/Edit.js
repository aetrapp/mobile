import _ from "lodash";
import moment from "moment";
import { connect } from "react-redux";
import React from "react";

// Components & Styles
import { Alert, Keyboard } from "react-native";
import {
  Container,
  Content,
  Form,
  Label,
  Input,
  Item,
  Picker
} from "native-base";
import { TextInputMask } from 'react-native-masked-text';
import { Spinner } from "components";

// Actions
import { patchProfile } from "actions/auth";

// Validators
import {
  validateFirstName,
  validateLastName,
  validateCellphone,
  validateBrDate
} from "helpers/validators";

// Error helper
import showErrorAlert from "helpers/error-alert";

class EditProfile extends React.Component {
  constructor(props) {
    super(props);

    // Navigator
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onChangeField = this.onChangeField.bind(this);
    this.submit = this.submit.bind(this);

    // State
    this.state = {
      values: this.props.user,
      spinnerVisible: false
    };
  }

  componentDidMount() {
    this.refs.firstNameInput._root.focus();
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {

      // Refresh values from TextMaskInput components . On Android, "onBlur" do not 
      // fire this event ("next") when input is focused.
      const cellphoneNumberInput = this.refs['cellphoneNumberInput'].getElement()._root;
      if (cellphoneNumberInput.isFocused()) {
        cellphoneNumberInput.blur();
        this.onChangeField('cellphoneNumber', this.refs['cellphoneNumberInput'].getRawValue().toString());
      }

      const landlineNumberInput = this.refs['landlineNumberInput'].getElement()._root;
      if (landlineNumberInput.isFocused()) {
        landlineNumberInput.blur();
        this.onChangeField('landlineNumber', this.refs['landlineNumberInput'].getRawValue().toString());
      }

      const dateOfBirthInput = this.refs['dateOfBirthInput'].getElement()._root;
      if (dateOfBirthInput.isFocused()) {
        const dateOfBirthInputValue = this.refs['dateOfBirthInput'].getRawValue();
        this.onChangeField('dateOfBirth', dateOfBirthInputValue.isValid() ? dateOfBirthInputValue.format('DD/MM/YYYY') : null);
      }

      Keyboard.dismiss();

      this.submit();
    }
  }

  async submit() {
    const {
      firstName,
      lastName,
      cellphoneNumber,
      dateOfBirth
    } = this.state.values;

    const firstNameResult = validateFirstName(firstName);
    if (!firstNameResult.isValid()) {
      Alert.alert('Preenchimento incorreto', firstNameResult.firstError().message);
      return;
    }

    const lastNameResult = validateLastName(lastName);
    if (!lastNameResult.isValid()) {
      Alert.alert('Preenchimento incorreto', lastNameResult.firstError().message);
      return;
    }

    const cellphoneResult = validateCellphone(cellphoneNumber);
    if (!cellphoneResult.isValid()) {
      Alert.alert('Preenchimento incorreto', cellphoneResult.firstError().message);
      return;
    }

    const dateofBirthResult = validateBrDate(dateOfBirth);
    if (!dateofBirthResult.isValid()) {
      Alert.alert('Preenchimento incorreto', dateofBirthResult.firstError().message);
      return;
    }
    
    try {
      this.setState({ spinnerVisible: true });
      await this.props.patchProfile(this.props.user.id, this.state.values);
      this.setState({ spinnerVisible: false });
      setTimeout(() => {
        Alert.alert(
          'Dados alterados!',
          'Seu perfil foi atualizado na plataforma.',
          [{ text: 'OK', onPress: () => this.props.navigator.popToRoot() }],
          { cancelable: false }
        );
      }, 10);
    } catch (error) {
      this.setState({ spinnerVisible: false });
      setTimeout(() => {
        showErrorAlert(error);
      }, 10);
    }
  }

  onChangeField(fieldName, value) {
    let state = { ...this.state };
    state.values[fieldName] = value;
    this.setState(state);
  }

  render() {
    const {
      dateOfBirth,
      cellphoneNumber,
      landlineNumber,
      firstName,
      gender,
      lastName
    } = this.state.values;

    return (
      <Container>
        <Spinner visible={this.state.spinnerVisible} />
        <Content>
          <Form>
            <Item>
              <Label>Nome</Label>
              <Input
                value={firstName}
                ref='firstNameInput'
                autoCapitalize='words'
                returnKeyType='next'
                onSubmitEditing={() => { this.refs.lastNameInput._root.focus(); }}
                onChangeText={text => this.onChangeField('firstName', text)}
              />
            </Item>
            <Item>
              <Label>Sobrenome</Label>
              <Input
                value={lastName}
                ref='lastNameInput'
                autoCapitalize='words'
                returnKeyType='next'
                onSubmitEditing={() => { this.refs['cellphoneNumberInput'].getElement()._root.focus(); }}
                onChangeText={text => this.onChangeField('lastName', text)}
              />
            </Item>
            <Item>
              <Label>Celular</Label>
              <TextInputMask
                ref='cellphoneNumberInput'
                value={cellphoneNumber}
                type={'cel-phone'}
                customTextInput={Input}
                returnKeyType='next'
                onSubmitEditing={() => {
                  this.refs['landlineNumberInput'].getElement()._root.focus();
                }}
                onBlur={() => {
                  this.onChangeField('cellphoneNumber', this.refs['cellphoneNumberInput'].getRawValue().toString());
                }}
              />
            </Item>
            <Item>
              <Label>Telefone fixo</Label>
              <TextInputMask
                ref="landlineNumberInput"
                value={landlineNumber}
                type={'cel-phone'}
                customTextInput={Input}
                returnKeyType='next'
                onSubmitEditing={() => { this.refs['dateOfBirthInput'].getElement()._root.focus(); }}
                onBlur={() => {
                  this.onChangeField('landlineNumber', this.refs['landlineNumberInput'].getRawValue().toString());
                }}
              />
            </Item>
            <Item>
              <Label>Nascimento</Label>
              <TextInputMask
                ref='dateOfBirthInput'
                type={'datetime'}
                options={{
                  format: 'DD/MM/YYYY'
                }}
                value={dateOfBirth}
                customTextInput={Input}
                onBlur={() => {
                  const inputValue = this.refs['dateOfBirthInput'].getRawValue();
                  this.onChangeField('dateOfBirth', inputValue.isValid() ? inputValue.format('DD/MM/YYYY') : null);
                }}
                returnKeyType='next'
              />
            </Item>
            <Item>
              <Label>Gênero</Label>
              <Picker
                ref='genderPicker'
                mode="dropdown"
                style={{ flex: 1 }}
                selectedValue={gender || "undisclosed"}
                onValueChange={value => { this.onChangeField('gender', value); }}
              >
                <Picker.Item label="Feminino" value="female" />
                <Picker.Item label="Masculino" value="male" />
                <Picker.Item label="Outro" value="other" />
                <Picker.Item label="Não informado" value="undisclosed" />
              </Picker>
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  let user = _.pick(state.auth.user, ['id', 'firstName', 'lastName', 'cellphoneNumber', 'landlineNumber', 'dateOfBirth', 'gender']);
  if (user.dateOfBirth) user.dateOfBirth = moment(user.dateOfBirth).format('DD/MM/YYYY');
  return {
    user
  };
};

const mapDispatchToProps = {
  patchProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
