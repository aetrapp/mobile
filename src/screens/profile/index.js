import moment from "moment";
import { connect } from "react-redux";
import React from "react";

// Mask Service
import { MaskService } from 'react-native-masked-text';

// Gender helper
import translateGender from 'helpers/translate-gender';

// Style
import { navigatorStyle } from "styles";

// Components
import { Alert, View } from "react-native";
import {
  Body,
  Icon,
  List,
  ListItem,
  Container,
  Content,
  Right,
  Text
} from "native-base";

// Actions
import { signOut } from "actions/auth";
import navigationOptions from "screens/navigationOptions";

class Profile extends React.Component {
  static navigatorStyle = navigatorStyle;

  constructor(props) {
    super(props);

    // Bindings
    this.editProfile = this.editProfile.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  editProfile(property) {
    switch (property) {
      case "password":
        this.props.navigator.push({
          screen: "aetrapp.Profile.Edit.Password"
        });
        break;
    }
  }

  signOut() {
    Alert.alert(
      "Finalizar sessão",
      "Deseja realmente sair?",
      [
        { text: 'Cancelar', style: 'cancel' },
        {
          text: 'OK', onPress: () => {
            this.props.signOut();
          }
        }
      ]
    );
  }

  render() {
    const { user } = this.props;

    if (typeof user != "object")
      return <View></View>;
    else
      return (
        <Container>
          <Content>
            <List>
              <ListItem itemDivider></ListItem>
              <ListItem>
                <Body>
                  <Text note>Nome</Text>
                  <Text>{user.firstName} {user.lastName}</Text>
                </Body>
              </ListItem>
              <ListItem>
                <Body>
                  <Text note>Gênero</Text>
                  <Text>{translateGender(user.gender)}</Text>
                </Body>
              </ListItem>
              <ListItem>
                <Body>
                  <Text note>Data de nascimento</Text>
                  <Text>{user.dateOfBirth && moment(user.dateOfBirth).format('DD/MM/YYYY')}</Text>
                </Body>
              </ListItem>
              <ListItem>
                <Body>
                  <Text note>E-mail</Text>
                  <Text>{user.email}</Text>
                </Body>
              </ListItem>
              <ListItem>
                <Body>
                  <Text note>Telefone celular</Text>
                  <Text>{user.cellphoneNumber && MaskService.toMask('cel-phone', user.cellphoneNumber)}</Text>
                </Body>
              </ListItem>
              <ListItem>
                <Body>
                  <Text note>Telefone fixo</Text>
                  <Text>{user.landlineNumber && MaskService.toMask('cel-phone', user.landlineNumber)}</Text>
                </Body>
              </ListItem>

              <ListItem itemDivider>
              </ListItem>
              <ListItem button onPress={() => {
                this.props.navigator.push({
                  ...navigationOptions("aetrapp.Profile.Edit")
                });
              }}>
                <Body>
                  <Text>Editar perfil</Text>
                </Body>
                <Right>
                  <Icon name="create" />
                </Right>
              </ListItem>
              <ListItem button onPress={() => { this.editProfile('password'); }}>
                <Body>
                  <Text>Alterar senha</Text>
                </Body>
                <Right>
                  <Icon name="key" />
                </Right>
              </ListItem>
              <ListItem button onPress={this.signOut} >
                <Body>
                  <Text>Finalizar sessão</Text>
                </Body>
                <Right>
                  <Icon name="exit" />
                </Right>
              </ListItem>
            </List>
          </Content>
        </Container>
      );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  };
};

const mapDispatchToProps = {
  signOut
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
