// Redux & React
import { connect } from "react-redux";
import React, { Component } from "react";

// Other modules
import _ from "lodash";
import t from "tcomb-validation";

// Components & Styles
import { colors, navigatorStyle } from "styles";
import { Alert } from "react-native";
import {
  Container,
  Form,
  Label,
  Input,
  Item,
  Text
} from "native-base";
import { Spinner } from "components";

// Service
import client from "services/helpers/client";
const authManagement = client.service("authManagement");

// Actions
import { updateCredentials } from "actions/auth";

class SignUp extends React.Component {
  static navigatorStyle = {
    ...navigatorStyle,
    tabBarHidden: true
  };

  constructor(props) {
    super(props);

    // Navigator
    this.props.navigator.setTitle({ title: "Edite sua senha" });
    this.props.navigator.setButtons({
      rightButtons: [{
        title: 'Salvar',
        id: 'next'
      }]
    });
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onChange = this.onChange.bind(this);
    this.validate = this.validate.bind(this);

    // State
    this.state = {
      spinnerVisible: false,
      values: {},
      errors: {},
      touched: {}
    };
  }

  onNavigatorEvent(event) {
    if (event.id == "next") {
      if (this.validate()) {
        this.setState({ spinnerVisible: true });
        const { user } = this.props;
        authManagement
          .create({
            action: 'passwordChange',
            value: {
              oldPassword: this.state.values.currentPassword,
              password: this.state.values.password,
              user: {
                email: user.email
              }
            }
          })
          .then(res => {
            this.props.updateCredentials({ password: this.state.values.password });
            Alert.alert("Alteração", "Sua senha foi alterada com sucesso.", [
              { text: 'OK', onPress: () => this.props.navigator.pop() },
            ], { cancelable: false });
          })
          .catch(err => {
            if (err && err.errors && err.errors.oldPassword) {
              Alert.alert("Erro", "Senha atual não confere");
            } else {
              Alert.alert("Erro", err.message);
            }
          })
          .finally(() => {
            this.setState({ spinnerVisible: false });
          });
      };
    }
  }

  validate() {
    var newState = { ...this.state };
    const { currentPassword, password, confirmPassword } = this.state.values;
    const { credentials } = this.props;

    // Validation refinement
    const PasswordString = t.refinement(t.String, function (s) {
      return s && s.length >= 6 && s.length < 50;
    });

    if (!t.validate(currentPassword, PasswordString).isValid()) {
      Alert.alert('Erro', 'A senha atual inválida');
    } else if (!(t.validate(password, PasswordString).isValid())) {
      Alert.alert('Erro', 'A nova senha deve ter no mínimo 6 caracteres');
    } else if (password != confirmPassword) {
      Alert.alert('Erro', 'A confirmação não coincide');
    }

    // Verify each field
    newState.errors.currentPassword = !(t.validate(currentPassword, PasswordString).isValid());
    newState.errors.password = !(t.validate(password, PasswordString).isValid());
    newState.errors.confirmPassword = !(t.validate(confirmPassword, PasswordString).isValid());

    // All fields should be considered touched
    newState.touched.currentPassword = true;
    newState.touched.password = true;
    newState.touched.confirmPassword = true;

    // Update state
    this.setState(newState);

    // Count errors
    const errors = _.filter(newState.errors, v => { return v }).length;

    return errors == 0;
  }

  onChange(fieldName, text) {
    var newState = { ...this.state };
    newState.touched[fieldName] = true;
    newState.values[fieldName] = text;
    this.setState(newState);
  }

  render() {
    const { currentPassword, password, confirmPassword } = this.state.values;
    const { errors, touched } = this.state;
    return (
      <Container>
        <Spinner visible={this.state.spinnerVisible} />
        <Form>
          <Item floatingLabel >
            <Label>Senha atual</Label>
            <Input
              type="currentPassword"
              value={currentPassword}
              secureTextEntry
              onChangeText={text => this.onChange("currentPassword", text)}
            />
          </Item>
          <Item floatingLabel >
            <Label>Nova senha (mínimo de 6 caracteres)</Label>
            <Input
              type="password"
              value={password}
              secureTextEntry
              onChangeText={text => this.onChange("password", text)}
            />
          </Item>
          <Item floatingLabel error={password != confirmPassword} >
            <Label>Confirme a nova senha</Label>
            <Input
              type="confirmPassword"
              value={confirmPassword}
              secureTextEntry
              onChangeText={text => this.onChange("confirmPassword", text)}
            />
          </Item>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    credentials: state.auth.credentials,
    user: state.auth.user
  };
};

const mapDispatchToProps = {
  updateCredentials
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
