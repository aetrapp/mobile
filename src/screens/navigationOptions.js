import _ from "lodash";
import { colors } from "styles";

const defaultNavigatorStyle = {
  navBarBackgroundColor: colors.tundora,
  navBarButtonColor: colors.apple,
  navBarNoBorder: true,
  navBarTextColor: colors.apple,
  navBarButtonFontSize: 14,
  topBarElevationShadowEnabled: false,
};

/*
 * Returns navigation options for pre-defined screen names
 */
export default function navigationOptions(name) {

  const screen = {
    "aetrapp.Auth.SignIn": {
      title: "Entrar",
      navigatorStyle: {},
      navigatorButtons: {
        rightButtons: [{
          title: 'Esqueci a senha',
          id: 'next'
        }]
      }
    },
    "aetrapp.Auth.SignUp.Step1": {
      title: "Cadastro de usuário",
      navigatorButtons: {
        rightButtons: [{
          title: 'Seguinte',
          id: 'next'
        }]
      }
    },
    "aetrapp.Auth.SignUp.Step2": {
      title: "Definir senha",
      navigatorButtons: {
        rightButtons: [{
          title: 'Concluir cadastro',
          id: 'next'
        }]
      }
    },
    "aetrapp.Traps.Create.PictureSource": {
      title: "Nova armadilha",
      navigatorStyle: {
        tabBarHidden: true
      },
      navigatorButtons: {}
    },
    "aetrapp.Traps.Create.TakePicture": {
      title: "",
      navigatorStyle: {
        drawUnderNavBar: true,
        navBarTransparent: true,
        navBarTranslucent: true,
        navBarButtonColor: "white",
        topBarElevationShadowEnabled: false,
        tabBarHidden: true,
        backButtonHidden: true
      },
      navigatorButtons: {}
    },
    "aetrapp.Traps.Create.ReviewPicture": {
      title: "",
      navigatorStyle: {
        drawUnderNavBar: true,
        navBarTransparent: true,
        navBarTranslucent: true,
        navBarButtonColor: "white",
        topBarElevationShadowEnabled: false,
        tabBarHidden: true
      },
      navigatorButtons: {
        rightButtons: [
          {
            title: "Avançar",
            id: "next"
          }
        ]
      }
    },
    "aetrapp.Traps.Create.Location": {
      title: "Local da Armadilha",
      navigatorStyle: {
        tabBarHidden: true
      },
      navigatorButtons: {
        rightButtons: [
          {
            title: "Seguinte",
            id: "next"
          }
        ]
      }
    },
    "aetrapp.Traps.Create.City": {
      title: "Localidade",
      navigatorStyle: {
        tabBarHidden: true
      },
      navigatorButtons: {
        rightButtons: [
          {
            title: "Seguinte",
            id: "next"
          }
        ]
      }
    },
    "aetrapp.Traps.Create.Address": {
      title: "Endereço",
      navigatorStyle: {
        tabBarHidden: true
      },
      navigatorButtons: {
        rightButtons: [
          {
            title: "Salvar",
            id: "next"
          }
        ]
      }
    },
    "aetrapp.Traps.Show": {
      title: "Informações da armadilha",
      navigatorStyle: {
        tabBarHidden: true
      }
    },    
    "aetrapp.ShowPicture": {
      navigatorStyle: {
        drawUnderNavBar: true,
        navBarButtonColor: "white",
        navBarTranslucent: true,
        navBarTransparent: true,
        tabBarHidden: true,
        topBarElevationShadowEnabled: false
      }
    },
    "aetrapp.Traps.Map.Options": {
      title: "Configurações do mapa",
      navigatorStyle: {
        tabBarHidden: true
      }
    },
    "aetrapp.Samples.Assistant.SelectSource": {
      title: "Imagem da amostra",
      navigatorStyle: {
        tabBarHidden: true
      }
    },
    "aetrapp.Samples.Assistant.TakePicture": {
      navigatorStyle: {
        drawUnderNavBar: true,
        navBarTransparent: true,
        navBarTranslucent: true,
        navBarButtonColor: "white",
        topBarElevationShadowEnabled: false,
        tabBarHidden: true
      }
    },
    "aetrapp.Samples.Assistant.ReviewPicture": {
      navigatorStyle: {
        drawUnderNavBar: true,
        navBarButtonColor: "white",
        navBarTranslucent: true,
        navBarTransparent: true,
        tabBarHidden: true,
        topBarElevationShadowEnabled: false
      },
      navigatorButtons: {
        rightButtons: [
          {
            title: "Enviar",
            id: "submit"
          }
        ]
      }
    },
    "aetrapp.Samples.Show": {
      title: "Detalhes da amostra",
      navigatorStyle: {
        tabBarHidden: true
      }
    },
    "aetrapp.Samples.Picture.Show": {
      navigatorStyle: {
        drawUnderNavBar: true,
        navBarButtonColor: "white",
        navBarTranslucent: true,
        navBarTransparent: true,
        tabBarHidden: true,
        topBarElevationShadowEnabled: false
      }
    },
    "aetrapp.Notifications.Show": {
      title: "Notificação",
      navigatorStyle: {
        tabBarHidden: true
      }
    },
    "aetrapp.Profile.Edit": {
      title: "Edição de perfil",
      navigatorStyle: {
        tabBarHidden: true
      },
      navigatorButtons: {
        rightButtons: [{
          title: 'Alterar',
          id: 'next'
        }]
      }
    },
    
  };

  const options = {
    screen: name,
    title: screen[name].title,
    backButtonTitle: '',
    navigatorStyle: _.merge(
      {},
      defaultNavigatorStyle,
      screen[name].navigatorStyle
    ),
    navigatorButtons: screen[name].navigatorButtons
  };

  return options;
}