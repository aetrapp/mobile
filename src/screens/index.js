import { Navigation, ScreenVisibilityListener } from "react-native-navigation";

// Picture
import ShowPicture from "./ShowPicture";

// Auth
import Welcome from "./Welcome";
import SignIn from "./auth/SignIn";
import SignUpStep1 from "./auth/signup/Step1";
import SignUpStep2 from "./auth/signup/Step2";
import SignOut from "./auth/SignOut";

// Traps
import Traps from "./traps";
import ShowTrap from "./traps/Show";
import TrapsMap from "./traps/Map";
import TrapsMapOptions from "./traps/Map/Options";

// Traps Assistant
import CreateTrapAddress from "./traps/create/Address";
import CreateTrapCity from "./traps/create/City";
import CreateTrapLocation from "./traps/create/Location";
import CreateTrapPictureSource from "./traps/create/PictureSource";
import CreateTrapTakePicture from "./traps/create/TakePicture";
import CreateTrapReviewPicture from "./traps/create/ReviewPicture";

// Samples
import SampleList from "./samples";
import SampleShow from "./samples/Show";
import SamplePictureShow from "./samples/ShowPicture";

// Samples Assistant
import SampleAssistantSelectSource from "./samples/assistant/SelectSource";
import SampleAssistantTakePicture from "./samples/assistant/TakePicture";
import SampleAssistantReviewPicture from "./samples/assistant/ReviewPicture";

// Notifications
import NotificationIndex from "./notifications";
import NotificationShow from "./notifications/Show";

// Profile
import Profile from "./profile";
import ProfileEditPassword from "./profile/Password";
import ProfileEdit from "./profile/Edit";

// import Guide from "./guide";
import HelpIndex from "./help";
import About from "./help/about";
import AssembleTrapGuide from "./help/guides/assembleTrap";
import CollectSampleGuide from "./help/guides/collectSample";

// register all screens of the app (including internal ones)
export function registerScreens(store, Provider) {
  
  // Auth
  Navigation.registerComponent("aetrapp.Auth.SignIn", () => SignIn, store, Provider);
  Navigation.registerComponent("aetrapp.Auth.SignUp.Step1", () => SignUpStep1, store, Provider);
  Navigation.registerComponent("aetrapp.Auth.SignUp.Step2", () => SignUpStep2, store, Provider);

  // Help
  Navigation.registerComponent("aetrapp.Help.About", () => About, store, Provider);
  Navigation.registerComponent("aetrapp.Help.Guides.AssembleTrap", () => AssembleTrapGuide, store, Provider);
  Navigation.registerComponent("aetrapp.Help.Guides.CollectSample", () => CollectSampleGuide, store, Provider);
  Navigation.registerComponent("aetrapp.Help.Index", () => HelpIndex, store, Provider);

  // Notifications    
  Navigation.registerComponent("aetrapp.Notifications.Index", () => NotificationIndex, store, Provider);
  Navigation.registerComponent("aetrapp.Notifications.Show", () => NotificationShow, store, Provider);

  // Profile
  Navigation.registerComponent("aetrapp.Profile.Edit.Password", () => ProfileEditPassword, store, Provider);
  Navigation.registerComponent("aetrapp.Profile", () => Profile, store, Provider);
  Navigation.registerComponent("aetrapp.Profile.Edit", () => ProfileEdit, store, Provider);

  // Samples
  Navigation.registerComponent("aetrapp.Samples.Assistant.ReviewPicture", () => SampleAssistantReviewPicture, store, Provider);
  Navigation.registerComponent("aetrapp.Samples.Assistant.SelectSource", () => SampleAssistantSelectSource, store, Provider);
  Navigation.registerComponent("aetrapp.Samples.Assistant.TakePicture", () => SampleAssistantTakePicture, store, Provider);
  Navigation.registerComponent("aetrapp.Samples.List", () => SampleList, store, Provider);
  Navigation.registerComponent("aetrapp.Samples.Picture.Show", () => SamplePictureShow, store, Provider);
  Navigation.registerComponent("aetrapp.Samples.Show", () => SampleShow, store, Provider);

  // Show Picture
  Navigation.registerComponent("aetrapp.ShowPicture", () => ShowPicture, store, Provider);

  // Sign Out
  Navigation.registerComponent("aetrapp.SignOut", () => SignOut, store, Provider);

  // Traps
  Navigation.registerComponent("aetrapp.Traps.Create.Address", () => CreateTrapAddress, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Create.City", () => CreateTrapCity, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Create.Location", () => CreateTrapLocation, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Create.PictureSource", () => CreateTrapPictureSource, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Create.ReviewPicture", () => CreateTrapReviewPicture, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Create.TakePicture", () => CreateTrapTakePicture, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Map", () => TrapsMap, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Map.Options", () => TrapsMapOptions, store, Provider);
  Navigation.registerComponent("aetrapp.Traps.Show", () => ShowTrap, store, Provider);
  Navigation.registerComponent("aetrapp.Traps", () => Traps, store, Provider);

  // Welcome
  Navigation.registerComponent("aetrapp.Welcome", () => Welcome, store, Provider);  
}

export function registerScreenVisibilityListener() {
  new ScreenVisibilityListener({
    /* eslint-disable no-console */
    willAppear: ({ screen }) => console.log(`Displaying screen ${screen}`),
    didAppear: ({ screen, startTime, endTime, commandType }) =>      
      console.log(
        "screenVisibility",
        `Screen ${screen} displayed in ${endTime -
        startTime} millis [${commandType}]`
      ),
    willDisappear: ({ screen }) =>
      console.log(`Screen will disappear ${screen}`),
    didDisappear: ({ screen }) => console.log(`Screen disappeared ${screen}`)
    /* eslint-enable no-console */
  }).register();
}