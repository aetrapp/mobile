import React from "react";

// Screens
import navigationOptions from "screens/navigationOptions";

// Components
import Video from "react-native-video";
import { Platform, StyleSheet } from "react-native";
import {
  Button,
  Container,
  Content,
  Text
} from "native-base";

// Style
import { colors } from "styles";
var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    backgroundColor: "#fffffc",
  },
  button: {
    margin: 2,
    backgroundColor: colors.apple,
    borderRadius: 0
  },
  backgroundVideo: {
    backgroundColor: Platform.OS === "ios" ? "#fffffc" : undefined,
    position: "absolute",
    top: -100,
    left: 0,
    bottom: 0,
    right: 0
  }
});

class Welcome extends React.Component {
  static navigatorStyle = {
    navBarHidden: true
  };

  constructor(props) {
    super(props);

    // Init state
    this.state = { 
      email: '',
      visibleButtons: false 
    };
  }

  render() {
    return (
      <Container style={styles.container}>
        <Video
          repeat={false}
          muted={true}
          resizeMode="contain"
          style={styles.backgroundVideo}
          source={require("../../assets/intro.mp4")}
          onLoad={() => setTimeout(() => {
            this.setState({ visibleButtons: true });
          }, 1000)}
        />
        {
          this.state.visibleButtons &&
          <Content padder>
            <Button
              block
              style={styles.button}
              onPress={() => this.props.navigator.push(navigationOptions("aetrapp.Auth.SignIn"))}
            >
              <Text>Entrar</Text>
            </Button>
            <Button
              block
              style={styles.button}
              onPress={() => this.props.navigator.push(navigationOptions("aetrapp.Auth.SignUp.Step1"))}
            >
              <Text>Fazer Cadastro</Text>
            </Button>
          </Content>
        }
      </Container>
    );
  }
}

export default Welcome;
