import React from "react";

// App config
import config from "../../config.json";

// Components
import { View, Dimensions } from "react-native";
import Image from "react-native-transformable-image-next";

class ShowPicture extends React.Component {
  constructor(props) {
    super(props);

    // Navigation
    this.onLayout = this.onLayout.bind(this);

    // State
    const { width, height } = Dimensions.get("window");
    this.state = {
      width,
      height
    };
  }

  onLayout() {
    const { width, height } = Dimensions.get("window");
    this.setState({
      width,
      height
    });
  }


  render() {
    const { blobId } = this.props;
    const { width, height } = this.state;
    const uri = `${config.API_URL}/files/${blobId}`;

    return (
      <View style={{ backgroundColor: "black" }} onLayout={this.onLayout}>
        <Image
          style={{ width, height }}
          source={{ uri }}
        />
      </View>
    );
  }
}

export default ShowPicture;
