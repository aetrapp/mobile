import _ from "lodash";

// Components
import React from "react";
import {
  Dimensions,
  FlatList,
  Image
} from "react-native";
import {
  Body,
  Card,
  CardItem,
  Container,
  Content,
  H2,
  Left,
  Text,
  View
} from "native-base";

// Style
import { navigatorStyle } from "styles";

class Guide extends React.Component {
  static navigatorStyle = {
    ...navigatorStyle,
    tabBarHidden: true
  };

  constructor(props) {
    super(props);
    this.props.navigator.setTitle({ title: "Como construir uma armadilha" });

    // add key number to cards
    cards = _.map(cards, (card, i) => {
      card.key = (i + 1).toString();
      return card;
    });
  }

  render() {
    const { width } = Dimensions.get("window");
    return (
      <Container>
        <Content>
          <FlatList
            data={cards}
            renderItem={(card) => {
              const { item, index } = card;
              return <Card style={{ elevation: 3 }}>
                <CardItem>
                  <Left>
                    <Body>
                      <H2>
                        Passo {index + 1}/{cards.length}
                      </H2>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Image
                    style={{ height: width, flex: 1 }}
                    source={item.image}
                  />
                </CardItem>
                <CardItem>{item.instructions}</CardItem>
              </Card>;
            }}
          />
        </Content>
      </Container>
    );
  }
}

let cards = [
  {
    image: require("images/guides/assemble-trap/000.jpeg"),
    instructions: (
      <View>
        <Text>
          A ovitrampa é uma armadilha utilizada para capturar ovos de mosquitos
          Aedes.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/001.jpeg"),
    instructions: (
      <View>
        <Text>Recorte uma garrafa PET de 2 litros na altura de 13 cm.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          <Text>Não utilize </Text>
          <Text style={{ fontWeight: "bold" }}>garrafas de 1,5L</Text>
          <Text> ou </Text>
          <Text style={{ fontWeight: "bold" }}>garrafas coloridas</Text>
          <Text>, isso pode influenciar na </Text>
          <Text style={{ fontWeight: "bold" }}>atratividade da armadilha</Text>
          <Text>!</Text>
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/002.jpeg"),
    instructions: (
      <View>
        <Text>As duas partes serão utilizadas.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/003.jpeg"),
    instructions: (
      <View>
        <Text>
          Recorte o gargalo conforme desenhado para fazer o dispensador de
          capim, que serve para atrair os moquitos, e recorte também uma tira
          de aproximadamente 1,5 x 7 cm.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/004.jpeg"),
    instructions: (
      <View>
        <Text>O dispensador de capim e a tira devem ficar assim.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/005.jpeg"),
    instructions: (
      <View>
        <Text>Colha uma pequena porção de capim ou grama.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/006.jpeg"),
    instructions: (
      <View>
        <Text>Pique o capim ou grama em pedaços bem pequenos.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/007.jpeg"),
    instructions: (
      <View>
        <Text>
          Recorte um quadrado de aproximadamente 6 x 6 cm de um filtro de café.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/008.jpeg"),
    instructions: (
      <View>
        <Text>As partes do dispensador de capim estão prontas!</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/009.jpeg"),
    instructions: (
      <View>
        <Text>Preencha a tampa da garrafa com o capim ou grama picada.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/010.jpeg"),
    instructions: (
      <View>
        <Text>Coloque o pedaço do filtro de café sobre a tampa com capim.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          <Text>Verifique se a tampa e o gargalo estão </Text>
          <Text style={{ fontWeight: "bold" }}>secos</Text>
          <Text> para </Text>
          <Text style={{ fontWeight: "bold" }}>não rasgar o papel</Text>
          <Text>.</Text>
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/011.jpeg"),
    instructions: (
      <View>
        <Text>
          Rosqueie o gargalo na tampa com cuidado para não rasgar o papel.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/012.jpeg"),
    instructions: (
      <View>
        <Text>Recorte o excesso de papel das bordas.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/013.jpeg"),
    instructions: (
      <View>
        <Text>
          O dispensador de capim está pronto! Ele vai funcionar como um saquinho
          de chá na água.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/014.jpeg"),
    instructions: (
      <View>
        <Text>Para fazer a paleta você precisará de:</Text>
        <Text>{`\u2022 `}Papel para aquarela;</Text>
        <Text>{`\u2022 `}Cartão de banco (54 x 86 mm);</Text>
        <Text>{`\u2022 `}Moeda de R$ 1,00;</Text>
        <Text>{`\u2022 `}Caneta esforgráfica azul.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          <Text>Procure na papelaria </Text>
          <Text style={{ fontWeight: "bold" }}>
            papel para aquarela com gramatura de 300g/m2
          </Text>
          <Text>, de cor </Text>
          <Text style={{ fontWeight: "bold" }}>branca ou creme</Text>
          <Text>.</Text>
        </Text>
        <Text>Cartolina e outros papéis não resistem à água.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/015.jpeg"),
    instructions: (
      <View>
        <Text>
          <Text>Use o cartão como molde e </Text>
          <Text style={{ fontWeight: "bold" }}>
            desenhe um retângulo no papel
          </Text>
          <Text>.</Text>
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          <Text>A caneta deve ser </Text>
          <Text style={{ fontWeight: "bold" }}>azul</Text>
          <Text> para que o sistema reconheça o círculo, e </Text>
          <Text style={{ fontWeight: "bold" }}>esferográfica</Text>
          <Text> para não borrar na água.</Text>
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/016.jpeg"),
    instructions: (
      <View>
        <Text>Coloque a moeda de R$ 1,00 no centro do retângulo.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/017.jpeg"),
    instructions: (
      <View>
        <Text>
          Usando a moeda como molde, desenhe um círculo azul no centro do
          retângulo.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          <Text>Confira se o círculo está </Text>
          <Text style={{ fontWeight: "bold" }}>completamente fechado</Text>
          <Text>, sem espaços em branco.</Text>
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/018.jpeg"),
    instructions: (
      <View>
        <Text>
          <Text style={{ fontWeight: "bold" }}>
            Recorte o retângulo cuidadosamente
          </Text>
          <Text>, tentando manter a borda azul na paleta.</Text>
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/019.jpeg"),
    instructions: (
      <View>
        <Text>A paleta está pronta!</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/020.jpeg"),
    instructions: (
      <View>
        <Text>
          A tira de plástico recortada anteriormente servirá para evitar o
          contato dos clips com o papel, para não manchá-lo de ferrugem.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/021.jpeg"),
    instructions: (
      <View>
        <Text>
          Fixe a paleta na borda do recipiente, não deixando os clips encostarem
          no papel.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/022.jpeg"),
    instructions: (
      <View>
        <Text>O dispensador de capim será fixado com outro clip.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/023.jpeg"),
    instructions: (
      <View>
        <Text>
          Fixe o dispensador de capim na borda oposta do recipiente. A tampa com
          capim deve ficar no fundo.
        </Text>
        <Text>A tampa com capim deve ficar no fundo.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/024.jpeg"),
    instructions: (
      <View>
        <Text>Agora falta revestir o recipiente com fita isolante preta.</Text>
        <Text>Desmonte a paleta e o dispensador de capim, por enquanto.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/025.jpeg"),
    instructions: (
      <View>
        <Text>Começando pelo fundo é mais fácil:</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/026.jpeg"),
    instructions: (
      <View>
        <Text>Vá colando tiras do centro para as bordas.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/027.jpeg"),
    instructions: (
      <View>
        <Text>Tente não deixar frestas:</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/028.jpeg"),
    instructions: (
      <View>
        <Text>Siga revestindo as laterais, circularmente.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/029.jpeg"),
    instructions: (
      <View>
        <Text>O recipiente deve ser revestido até o topo.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/030.jpeg"),
    instructions: (
      <View>
        <Text>
          Faça dois pequenos furos em lados opostos, bem próximos à borda.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/031.jpeg"),
    instructions: (
      <View>
        <Text>
          Fixe novamente a paleta e o dispensador de capim em bordas opostas.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/032.jpeg"),
    instructions: (
      <View>
        <Text>Sua ovitrampa está pronta.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/033.jpeg"),
    instructions: (
      <View>
        <Text>
          Certifique-se que a tampa com o capim ficaram no fundo e que os clips
          não encostam no papel.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/034.jpeg"),
    instructions: (
      <View>
        <Text>Utilize os furos laterais para fixar um pedaço de barbante.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/035.jpeg"),
    instructions: (
      <View>
        <Text>
          Preencha a armadilha com água limpa até o topo do círculo da paleta.
        </Text>
        <Text>
          O nível da água diminuirá durante a semana devido à evaporação.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          É normal a redução do nível e água por evaporação ao longo da semana.
        </Text>
        <Text style={{ fontWeight: "bold" }}>
          Não complete o nível d’água para não submergir os ovos!
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/036.jpeg"),
    instructions: (
      <View>
        <Text>
          Utilizando um prego, pendure a armadilha na parede em uma área externa
          da casa, bem protegida por telhado ou beiral.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/037.jpeg"),
    instructions: (
      <View>
        <Text>
          Importante: A armadilha deve estar protegida de sol excessivo,
          evitando evaporação muito rápida, e protegida de chuva e respingos,
          evitando sujeira na paleta.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          Busque um local bem protegido no quintal, como uma varanda ou uma
          parede com beiral grande.
        </Text>
        <Text style={{ fontWeight: "bold" }}>
          Não coloque a armadilha debaixo de lâmpadas, pois elas atraem insetos
          que acabarão caindo na armadilha.
        </Text>
        <Text>Quanto mais escuro o local da armadilha, melhor.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/037.jpeg"),
    instructions: (
      <View>
        <Text>
          A altura da boca da armadilha deve estar entre 80 cm e 1 m do chão.
        </Text>
        <Text>Oriente crianças a não tocarem na armadilha.</Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/app-steps-select-my-traps-tab.jpg"),
    instructions: (
      <View>
        <Text>
          Utilizando o aplicativo AeTrapp, clique em “Armadilhas" no menu inferior.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/app-steps-create-new-trap.jpg"),
    instructions: (
      <View>
        <Text>
          Clique no símbolo “+”, no canto superior esquerdo, para cadastrar sua armadilha.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/assemble-trap/app-steps-enter-trap.jpg"),
    instructions: (
      <View>
        <Text>
          Fotografe sua armadilha no local onde foi instalada, preencha todos os dados requeridos e clique em confirmar: sua armadilha está cadastrada no sistema!
          Agora aguarde uma semana para fotografar a primeira amostra, você será notificado para não se esquecer.
          Muitas vezes não encontramos ovos nas duas primeiras semanas. Isso é normal, não desanime!
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          Não deixe de ler o tutorial para fotografia de amostras. Para que o sistema consiga reconhecer as fotografias e contar os ovos,
          é extremamente importante que as orientações de fotografia sejam  seguidas corretamente.
        </Text>
      </View>
    )
  }
];

export default Guide;
