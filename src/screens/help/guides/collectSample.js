import _ from "lodash";

// Components
import React from "react";
import {
  Dimensions,
  FlatList,
  Image,
} from "react-native";
import {
  Body,
  Card,
  CardItem,
  Content,
  Container,
  H2,
  Left,
  Text,
  View
} from "native-base";

// Style
import { navigatorStyle } from "styles";

class Guide extends React.Component {
  static navigatorStyle = {
    ...navigatorStyle,
    tabBarHidden: true
  };

  constructor(props) {
    super(props);
    this.props.navigator.setTitle({ title: "Guia de Coleta da Amostra" });

    // add key number to cards
    cards = _.map(cards, (card, i) => {
      card.key = (i + 1).toString();
      return card;
    });
  }

  render() {
    const { width } = Dimensions.get("window");
    return (
      <Container>
        <Content>
          <FlatList
            data={cards}
            renderItem={(card) => {
              const { item, index } = card;
              return <Card style={{ elevation: 3 }}>
                <CardItem>
                  <Left>
                    <Body>
                      <H2>
                        Passo {index + 1}/{cards.length}
                      </H2>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Image
                    style={{ height: width, flex: 1 }}
                    source={item.image}
                  />
                </CardItem>
                <CardItem>{item.instructions}</CardItem>
              </Card>;
            }}
          />
        </Content>
      </Container>
    );
  }
}

let cards = [
  {
    image: require("images/guides/collect-sample/000.jpeg"),
    instructions: (
      <View>
        <Text>
          Após cadastrar sua armadilha, você receberá semanalmente um lembrete para fotografar sua amostra. Siga as orientações para tirar fotos com qualidade, assim o sistema identificará com mais precisão o número de ovos.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/001.jpeg"),
    instructions: (
      <View>
        <Text>
          A paleta deve ser removida antes do descarte do líquido da armadilha.
      </Text>
        <Text>
          Remova a paleta com cuidado para os ovos não se soltarem.
      </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          Mesmo não havendo ovos visíveis você deve fotografar a paleta e enviar para o sistema
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/002.jpeg"),
    instructions: (
      <View>
        <Text>
          Coloque a paleta em um recipiente limpo.
          </Text>
        <Text>
          A paleta deve estar limpa, mas eventualmente pode haver sujeiras grandes como folhas e insetos mortos.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/003.jpeg"),
    instructions: (
      <View>
        <Text>
          Se houver sujeiras grandes, remova-as cuidadosamente.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/004.jpeg"),
    instructions: (
      <View>
        <Text>
          Deixe a paleta secar completamente, no sol secará mais rápido.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/005.jpeg"),
    instructions: (
      <View>
        <Text>
          Após a secagem é possível que a paleta esteja um pouco deformada.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/006.jpeg"),
    instructions: (
      <View>
        <Text>
          Desamasse-a com cuidado, sem tocar a região com ovos, deixando-a o mais plana possível.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/007.jpeg"),
    instructions: (
      <View>
        <Text>
          Encontre um lugar bem iluminado para fazer a fotografia da paleta.
        </Text>
        <Text>
          Fotografe durante o dia.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/007.jpeg"),
    instructions: (
      <View>
        <Text>
          Uma bancada de frente para uma janela, sem incidência direta de sol na paleta, é um bom lugar.
        </Text>
        <Text>
          Não utilize flash ou iluminação artificial.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/008.jpg"),
    instructions: (
      <View>
        <Text>
          Abra o aplicativo AeTrapp e clique no ícone da câmera fotográfica.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/009.jpeg"),
    instructions: (
      <View>
        <Text>
          Coloque a paleta sobre um fundo de papel branco.
        </Text>
        <Text>
          Utilizando livros, faça um suporte para o celular conforme a foto.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/010.jpeg"),
    instructions: (
      <View>
        <Text>
          Ajuste a altura do celular acrescentando ou removendo livros, de modo que a paleta esteja totalmente enquadrada na tela.
        </Text>
        <Text>
          Você deve tirar a foto o mais próximo possível, mas sem cortar as bordas da paleta.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          Não se esqueça que a foto deve ser tirada sobre um fundo de papel branco.
        </Text>
        <Text>
          Caso contrário o sistema não reconhecerá a amostra.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/011.jpeg"),
    instructions: (
      <View>
        <Text>
          Para ajustar o foco, toque na tela sobre a borda do círculo azul.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/012.jpeg"),
    instructions: (
      <View>
        <Text>
          Certifique-se de que não há sombras do celular ou de outros objetos sobre a paleta e fotografe.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/012.jpeg"),
    instructions: (
      <View>
        <Text>
          Verifique se a fotografia ficou bem iluminada, com foco, e se a paleta saiu inteira, sem cortes.
        </Text>
        <Text>
          Se estiver tudo ok, clique em enviar.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          Após o envio da foto, aguarde alguns minutos até receber o resultado da análise.
        </Text>
        <Text>
          Caso a amostra não seja aceita, verifique a mensagem de erro e tire uma nova fotografia corrigindo o problema detectado.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/013.jpeg"),
    instructions: (
      <View>
        <Text>
          Após receber a mensagem de sucesso na análise, despeje água sanitária pura sobre os ovos para que eles não se desenvolvam.
        </Text>
        <Text>
          Descarte a paleta no lixo. Mesmo se a paleta não tiver ovos visíveis, não a reutilize.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/014.jpeg"),
    instructions: (
      <View>
        <Text>
          Derrame a água da armadilha na terra ou na grama, em local onde não se formam poças.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/015.jpeg"),
    instructions: (
      <View>
        <Text>
          Lave bem a armadilha com uma esponja, água e sabão, esfregando bem as paredes internas.
        </Text>
        <Text>
          Use sabão neutro e retire todo o limo da armadilha.
        </Text>
        <Text>
          Enxague bem!
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/016.jpeg"),
    instructions: (
      <View>
        <Text>
          Lave o dispensador de capim da mesma forma.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          Descarte corretamente a paleta e higienize bem a armadilha para evitar que os ovos se desenvolvam.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/017.jpeg"),
    instructions: (
      <View>
        <Text>
          Com a armadilha limpa, coloque uma paleta nova e recarregue o dispensador com capim
        </Text>
        <Text>
          Preencha a armadilha com água limpa até o topo do círculo azul
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/collect-sample/017.jpeg"),
    instructions: (
      <View>
        <Text>
          Instale a armadilha novamente, sempre no mesmo local.
        </Text>
        <Text>
          Repita o procedimento semanalmente e continue colaborando com o monitoramento de Aedes.
        </Text>
      </View>
    )
  },
  {
    image: require("images/guides/alert.png"),
    instructions: (
      <View>
        <Text>
          A qualidade das fotos é essencial para o funcionamento do sistema
          </Text>
        <Text>
          Siga sempre as orientações!
        </Text>
      </View>
    )
  },
];

export default Guide;
