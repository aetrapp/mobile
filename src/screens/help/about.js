import _ from "lodash";

// App version
import { version } from "../../../package.json";

// Style
import { StyleSheet } from "react-native";
import { navigatorStyle } from "styles";

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
  },
  header: {
    marginTop: 10
  },
  text: {
    textAlign: 'center',
    marginBottom: 10
  }
});

// Components
import React, { Component } from "react";
import { Dimensions, Linking, Image, TouchableOpacity } from "react-native";
import {
  Content,
  Container,
  H1 as NBH1,
  H2 as NBH2,
  Text as NBText,
} from "native-base";

function H1(props) {
  return (
    <NBH1 style={[styles.header, styles.text]}>
      {props.children}
    </NBH1>
  )
}

function H2(props) {
  return (
    <NBH2 style={[styles.header, styles.text]}>
      {props.children}
    </NBH2>
  )
}

function Text(props) {
  return (
    <NBText style={styles.text}>
      {props.children}
    </NBText>
  )
}

class About extends React.Component {
  static navigatorStyle = {
    ...navigatorStyle,
    tabBarHidden: true
  };

  constructor(props) {
    super(props);

    // Set application title
    this.props.navigator.setTitle({ title: "Sobre o AeTrapp" });
  }

  render() {

    // calculate image dimension
    let { width } = Dimensions.get("window");
    width = width * .80;
    const height = width * 642 / 1075;

    return (
      <Container>
        <Content padder contentContainerStyle={styles.content}>
          <H1>
            AeTrapp v{version}
          </H1>
          <Text>
            Website
          </Text>
          <TouchableOpacity onPress={() => { Linking.openURL('http://www.aetrapp.org') }}>
            <Text>
              www.aetrapp.org
            </Text>
          </TouchableOpacity>
          <H2>
            Realização
          </H2>
          <Image
            style={{ width, height, marginBottom: 30, marginTop: 20 }}
            source={require('../../../images/about/partners.png')}
          />
          <H2>
            Coordenação Institucional - WWF-Brasil
          </H2>
          <Text>
            Marcelo Oliveira
          </Text>
          <H2>
            Idealização e Coordenação Científica e Tecnológica
          </H2>
          <Text>
            Oda Scatolini
          </Text>
          <H2>
            Equipe WWF-Brasil
          </H2>
          <Text>
            Flavio Quental – Articulação Rio Branco/AC
          </Text>
          <Text>
            Osvaldo Gajardo – Gestão
          </Text>
          <Text>
            Fred Brandão – Comunicação
          </Text>
          <Text>
            Bia Vilela – Comunicação
          </Text>
          <Text>
            Bruna Veríssimo - Comunicação
          </Text>
          <Text>
            Ana Paula Silva Ferreira - Administrativo
          </Text>
          <Text>
            Lea Maria David - Administrativo
          </Text>

          <H2>
            Equipe Fiocruz-RO
          </H2>

          <Text>
            Dra. Genimar Rebouças Julião
          </Text>
          <Text>
            Dr. Ricardo de Godoi Mattos Ferreira
          </Text>
          <Text>
            Marlon Ferreira Simplício
          </Text>
          <Text>
            Hecylana Oliveira de Melo
          </Text>
          <Text>
            Flaviana de Lima Bezerra
          </Text>
          <Text>
            Flávia Mendes
          </Text>

          <H2>
            Equipe Fiocruz-RJ
          </H2>

          <Text>
            Dra. Gerusa Belo Gibson dos Santos
          </Text>
          <Text>
            Dra. Izabel Cristina dos Reis
          </Text>
          <Text>
            Dra. Nildimar Honório Rocha
          </Text>
          <Text>
            Carmen Fátima Pinheiro
          </Text>
          <Text>
            Célio da Silva Pinel
          </Text>
          <Text>
            Glaucio Rocha Pereira
          </Text>
          <H2>
            Equipe LabicBR
          </H2>
          <Text>
            Oda Scatolini - Coordenação e Processamento de Imagens
          </Text>
          <Text>
            Caio Cardoso Lucena - Geoprocessamento
          </Text>
          <Text>
            Dra. Gerusa Belo Gibson dos Santos - Saúde Pública
          </Text>
          <Text>
            Dra. Izabel Cristina dos Reis - Saúde Pública
          </Text>
          <Text>
            Johana Katherine Pacheco Tochoy - Design Gráfico
          </Text>
          <Text>
            Luis Felipe Caracas Palacios - Webdesign
          </Text>
          <Text>
            Whiston Kendrick Borja Reyna - Desenvolvimento
          </Text>
          <H2>
            Equipe Inovapps
          </H2>
          <Text>
            Oda Scatolini - Coordenação
          </Text>
          <Text>
            Luis Felipe Caracas Palacios - Webdesign
          </Text>
          <Text>
            Whiston Kendrick Borja Reyna - Desenvolvimento
          </Text>
          <H2>
            Visão computacional / Processamento de imagens
          </H2>
          <Text>
            João Paulo Herrera
          </Text>
          <H2>
            Aparabólica - Desenvolvimento
          </H2>
          <Text>
            Vitor George
          </Text>
          <Text>
            Miguel Peixe
          </Text>
          <H2>
            Instituto Paulo Freire - Material Pedagógico
          </H2>
          <Text>
            Moacir Gadotti - Presidente de Honra
          </Text>
          <Text>
            Ângela Antunes, Francisca Pini e Paulo Roberto Padilha - Diretores Pedagógicos
          </Text>
          <Text>
            Sonia Couto e Sheila Ceccon - Produção de Conteúdo
          </Text>
          <Text>
            Janaina Abreu - Coordenadora Gráfico-Editorial
          </Text>
          <H2>
            Imágica - Audiovisuais
          </H2>
          <Text>
            Beto Sporkens
          </Text>
          <H2>
            INCITI / UFPE - Mobilização e Formação
          </H2>
          <Text>
            Caio Scheidegger
          </Text>
          <Text>
            Circe Monteiro
          </Text>
          <Text>
            Maíra Brandão
          </Text>
          <Text>
            Violeta Assumpção
          </Text>

          <H2>
            Prefeitura Municipal de Recife - PE
          </H2>

          <Text>
            Dr. Jailson Correa - Secretário de Saúde
          </Text>
          <Text>
            Joanna Freire - Diretora de Vigilância
          </Text>
          <Text>
            Juliana Oriá
          </Text>
          <Text>
             Jurandir Almeida
          </Text>
          <Text>
            Maisa Belfort Teixeira
          </Text>
          <Text>
            Vania Nunes
          </Text>

          <H2>
            IFSP - Versão Beta - Piloto Rio Branco-AC - Abril 2017
          </H2>

          <Text>
            André Di Thommazo - Coordenação de Desenvolvimento
          </Text>
          <Text>
            Adenilson Noronha da Silva Junior - Desenvolvimento
          </Text>
          <Text>
            Estevão Henrique Coelho - Desenvolvimento
          </Text>
          <Text>
            Paulo Henrique Oliveira Santana - Desenvolvimento
          </Text>

          <H2>
            Prefeitura Municipal de Rio Branco - AC
          </H2>

          <Text>
            Oteniel Almeida - Secretário de Saúde
          </Text>
          <Text>
            Socorro Martins - Diretora de Vigilância Epidemiológica e Ambiental
          </Text>
          <Text>
            Luana Esteves - Diretora Geral de Vigilância em Saúde
          </Text>
          <Text>
            José Ferreira Neto (Louro) - Chefe Divisão de Endemias
          </Text>

          <H2>
            UFAC
          </H2>

          <Text>
            Dr. Ricardo da Costa Rocha
          </Text>

          <H2>
            IFAC
          </H2>

          <Text>
            Prof. Danielly Nóbrega
          </Text>
          <Text>
            Prof. Josina Maria Pontes Ribeiro
          </Text>

          <H2>
            Agradecimentos
          </H2>
          <Text>
            Dr. Fábio Gaiger (MS/OPAS), Dra. Marylene de Brito Arduino (SUCEN-SP), Pablo Pascale (SEGIB), Mariana Cancela (SEGIB), Silvia Rodrigues Follador (ponteAponte), Dr. Lúcio André de Castro Jorge (EMBRAPA)
          </Text>
          <Text>
          </Text>
        </Content>
      </Container>
    );
  }
}



export default About;
