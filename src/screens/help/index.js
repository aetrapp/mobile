

// Components
import React, { Component } from "react";
import { Linking } from "react-native";
import {
  Body,
  Container,
  Content,
  H1,
  Icon,
  Text,
  List,
  ListItem,
  Right
} from "native-base";

// Style
import { navigatorStyle } from "styles";

class Guide extends React.Component {
  static navigatorStyle = navigatorStyle;

  constructor(props) {
    super(props);

    // Navigator
    this.props.navigator.setTitle({ title: "Guias & Sobre" });

    // bindings
    this.pushScreen = this.pushScreen.bind(this);
  }

  pushScreen(screenName) {
    this.props.navigator.push({
      screen: screenName
    });
  }

  render() {
    return (
      <Container>
        <Content>
          <List>
            <ListItem itemDivider></ListItem>
            <ListItem button onPress={() => { this.pushScreen('aetrapp.Help.Guides.AssembleTrap') }}>
              <Body>
                <Text>Como construir uma armadilha</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem button onPress={() => { this.pushScreen('aetrapp.Help.Guides.CollectSample') }}>
              <Body>
                <Text>Como fotografar uma amostra</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem itemDivider></ListItem>
            <ListItem button onPress={() => { Linking.openURL('http://www.aetrapp.org') }}>
              <Body>
                <Text>Visite o website do AeTrapp</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem itemDivider></ListItem>
            <ListItem button onPress={() => { this.pushScreen('aetrapp.Help.About') }}>
              <Body>
                <Text>Sobre o AeTrapp</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container >
    );
  }
}

export default Guide;
