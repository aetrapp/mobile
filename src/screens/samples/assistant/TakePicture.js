import { connect } from "react-redux";
import React from "react";

// Navigation
import navigationOptions from "screens/navigationOptions";

// Components
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import { Spinner } from "components";
import Camera from "react-native-camera";
import Icon from "react-native-vector-icons/FontAwesome";

// Actions
import { updateData } from "actions/sampleAssistant";


class SelectSource extends React.Component {
  constructor(props) {
    super(props);

    // Bindings
    this.takePicture = this.takePicture.bind(this);

    // State
    this.state = {
      spinnerVisible: false,
      orientation: this.getOrientation()
    };
  }

  getOrientation() {
    const { width, height } = Dimensions.get("window");
    return width < height ? "portrait" : "landscape";
  }

  onLayout = () => {
    this.setState({ orientation: this.getOrientation() });
  };

  takePicture() {
    const options = {};

    this.setState({ spinnerVisible: true });

    return this.camera
      .capture({ metadata: options })
      .then(data => {
        this.setState({
          spinnerVisible: false
        });
        this.props.updateData({
          filePath: data.path,
          collectedAt: new Date()
        });
        this.props.navigator.push(navigationOptions("aetrapp.Samples.Assistant.ReviewPicture"));
      })
      .catch(err => {
        this.setState({ spinnerVisible: false });
        console.error("Error capturing picture", err);
      });
  }

  render() {
    const { orientation } = this.state;
    return (
      <View style={styles[orientation]} onLayout={this.onLayout}>
        <Spinner visible={this.state.spinnerVisible} />
        <Camera
          ref={cam => {
            this.camera = cam;
          }}
          captureTarget={Camera.constants.CaptureTarget.disk}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}
        />
        <View style={styles.snap}>
          <TouchableOpacity onPress={this.takePicture}>
            <Icon name="camera" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  portrait: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  landscape: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  preview: {
    ...StyleSheet.absoluteFillObject
  },
  snap: {
    margin: 30
  }
});

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  updateData
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectSource);
