import { connect } from "react-redux";
import React from "react";
import RNFS from "react-native-fs";

// Components
import { Alert, Dimensions, View } from "react-native";
import Image from "react-native-transformable-image-next";
import { Spinner } from "components";

// Actions
import { createSample, updateContext } from "actions/sampleAssistant";

// Helpers
import showErrorAlert from "helpers/error-alert";


class ReviewPicture extends React.Component {
  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.onLayout = this.onLayout.bind(this);

    // State
    const { width, height } = Dimensions.get("window");
    this.state = {
      spinnerVisible: false,
      width,
      height
    };
  }

  componentDidMount() {
    Alert.alert(
      "Revise a imagem",
      "Verifique se a imagem está bem iluminada, com foco correto e fundo branco. Caso não esteja adequada, volte à tela anterior e fotografe novamente."
    );
    this.props.updateContext({
      loadingFromGallery: false
    });
  }

  onLayout() {
    const { width, height } = Dimensions.get("window");
    this.setState({
      width,
      height
    });
  }

  async onNavigatorEvent(event) {
    if (event.id == "submit") {
      const self = this;
      this.setState({ spinnerVisible: true });

      // Read file
      const { filePath } = this.props.sample;
      let contents;
      try {
        contents = await RNFS.readFile(filePath, "base64");
      } catch (err) {
        showErrorAlert(err, () => {
          self.setState({ spinnerVisible: false });
        });
        return;
      }

      // Create sample
      try {
        await this.props.createSample({
          ...this.props.sample,
          base64: `data:image/jpeg;base64,${contents}`
        });
      } catch (err) {
        showErrorAlert(err, () => {
          self.setState({ spinnerVisible: false });
        });
        return;
      }

      this.setState({ spinnerVisible: false });

      // Due to a conflict between Alert and Spinner component on iOS,
      // setTimeout is used to wait for spinner to close
      setTimeout(() => {
        Alert.alert(
          "Imagem enviada com sucesso",
          "A análise pode levar alguns minutos. Aguarde o resultado antes de descartar a amostra, caso não seja reconhecida você poderá fotografá-la novamente.",
          [
            {
              text: "OK",
              onPress: () =>
                this.props.navigator.resetTo({
                  screen: "aetrapp.Traps"
                })
            }
          ],
          { cancelable: false }
        );
      }, 10);
    }
  }

  render() {
    const { filePath } = this.props.sample;
    const { width, height } = this.state;

    return (
      <View style={{ backgroundColor: "black" }} onLayout={this.onLayout}>
        <Spinner visible={this.state.spinnerVisible} />
        <Image
          style={{ width: width, height: height }}
          source={{ uri: filePath }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sample: state.sampleAssistant.data
  };
};

const mapDispatchToProps = {
  updateContext,
  createSample
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewPicture);
