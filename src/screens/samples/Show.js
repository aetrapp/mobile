import React from "react";
import { connect } from "react-redux";

// Navigation
import navigationOptions from "screens/navigationOptions";

// Localized moment
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Components
import { Spinner } from "components";
import { Alert, View } from "react-native";
import {
  Body,
  Content,
  Container,
  Icon,
  H2,
  Text,
  List,
  ListItem,
  Right
} from "native-base";

// Service
import client from "services/helpers/client";
const samplesService = client.service("samples");

// Actions
import { created } from "actions/samples";


class SampleView extends React.Component {

  constructor(props) {
    super(props);

    // Navigation
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // Bindings
    this.localizeStatus = this.localizeStatus.bind(this);
    this.showPicture = this.showPicture.bind(this);
  }

  // load sample if not available
  onNavigatorEvent(event) {
    if (event.id == "willAppear" && !this.props.sample) {
      samplesService.get(this.props.sampleId)
        .then(sample => {
          this.props.created(sample);
        })
        .catch(() => {
          Alert.alert("Amostra indisponível", "A amostra foi removida do sistema.", [
            { text: 'OK', onPress: () => this.props.navigator.pop() },
          ], { cancelable: false });
        });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // close screen when sample is removed  
    if (!nextProps.sample) this.props.navigator.pop();
  }

  shouldComponentUpdate(nextProps) {
    // avoid update when sample is removed
    return (typeof nextProps.sample !== "undefined");
  }

  showPicture() {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Samples.Picture.Show"),
      passProps: {
        sample: this.props.sample
      }
    });
  }

  localizeStatus(status) {
    switch (status) {
      case "unprocessed":
        return "Nenhuma análise foi iniciada até o momento.";
      case "analysing":
        return "Em execução...";
      case "invalid":
        return "Houve um erro.";
      case "valid":
        return "Finalizada com sucesso.";
      default:
        return "Houve um erro desconhecido.";
    }
  }

  fixInstructions(errorCode) {
    switch (errorCode) {
      case "ERR_001":
        return "Fotografe novamente com o celular bem apoiado para não tremer, em local com boa iluminação natural, conforme o tutorial. Toque a borda azul do círculo antes de fotografar para que a paleta fique em foco, e envie novamente.";
      case "ERR_002":
        return "Procure um lugar com boa iluminação natural, seguindo as orientações do tutorial. Fotografe e envie novamente.";
      case "ERR_003":
        return "Pode ser que o círculo não foi desenhado corretamente, não ficou completamente fechado ou não foi desenhado com caneta esferográfica azul e desbotou. Nesse caso, higienize a armadilha, substitua a paleta e envie nova amostra na próxima semana.";
      case "ERR_004":
        return "Procure um lugar com boa iluminação natural, verifique se não há sombras do celular ou de outros objetos na paleta. Fotografe e envie novamente, seguindo as orientações do tutorial.";
      case "ERR_005":
        return "Verifique se a fotografia foi tomada conforme o tutorial, com a câmera o mais próximo possível da paleta mas sem cortar as bordas. Fotografe e envie novamente.";
      case "ERR_006":
        return "Verifique se a fotografia foi tomada conforme o tutorial, com a câmera o mais próximo possível da paleta mas sem cortar as bordas. Fotografe e envie novamente. Caso a paleta não tenha sido desenhada com caneta esferográfica azul, higienize a armadilha, troque a paleta e envie nova amostra na próxima semana. O círculo azul deve estar totalmente fechado.";
      case "ERR_007":
        return "A água gera reflexos que dificultam a contagem dos ovos. Aguarde a paleta secar, fotografe conforme o tutorial e envie novamente.";
      case "ERR_008":
        return "O AeTrapp necessita de imagens com resolução mínima de 2000 x 1000 pixels. Verifique nas configurações de seu celular se a resolução está configurada corretamente. Caso seu celular não alcance essa resolução mínima, você precisará de um outro aparelho para realizar o monitoramento.";
      case "ERR_009":
        return "Verifique se a foto enviada é realmente a foto de uma paleta e se foi tirada sobre um fundo branco. Revise os tutoriais se necessário. Caso sua paleta não tenha sido feita corretamente, higienize a armadilha, troque a paleta e envie nova amostra na próxima semana.";
      case "ERR_010":
        return "Verifique se a fotografia foi tomada conforme o tutorial, com a câmera o mais próximo possível da paleta mas sem cortar as bordas. Fotografe e envie novamente.";
      default:
        return "O serviço de processamento de imagens retornou um erro desconhecido, contate o suporte.";
    }
  }

  render() {
    const { sample } = this.props;
    let analisysDuration;

    // check if sample is loaded
    if (sample && sample.analysisFinishedAt) {
      const duration = moment(sample.analysisFinishedAt).diff(sample.analysisStartedAt, 'minutes');
      analisysDuration = duration == 0 ? 'Menos de um minuto.' : (duration + ' minuto(s)');
    }

    return (
      <Container>

        <Spinner visible={!sample} />
        <Content>
          <ListItem itemDivider>
          </ListItem>

          {
            sample &&
            <List>
              {
                sample.status == "valid" &&
                <View>
                  <ListItem icon>
                    <Body>
                      <Text>Número de ovos</Text>
                    </Body>
                    <Right>
                      <Text>{sample.eggCount}</Text>
                    </Right>
                  </ListItem>
                </View>
              }

              <ListItem icon>
                <Body>
                  <Text>Data de coleta</Text>
                </Body>
                <Right>
                  <Text>{moment(sample.collectedAt).format('DD/MM/YY HH:mm')}</Text>
                </Right>
              </ListItem>

              <ListItem icon>
                <Body>
                  <Text selectable>Identificador</Text>
                </Body>
                <Right>
                  <Text>{sample.id}</Text>
                </Right>
              </ListItem>

              <ListItem icon button onPress={() => {
                this.props.navigator.push({
                  ...navigationOptions("aetrapp.Traps.Show"),
                  passProps: {
                    trapId: sample.trapId
                  }
                });
              }}>
                <Body>
                  <Text>Armadilha</Text>
                </Body>
                <Right>
                  <Text>{sample.trapId}</Text>
                </Right>
              </ListItem>

              <ListItem icon button onPress={this.showPicture}>
                <Body>
                  <Text>Ver imagem</Text>
                </Body>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>

              <ListItem itemDivider>
                <H2>Análise da imagem</H2>
              </ListItem>
              <ListItem itemDivider>
                <Text>
                  Resultado da análise:
                </Text>
              </ListItem>
              <ListItem>
                <Body>
                  <Text>{this.localizeStatus(sample.status)}</Text>
                </Body>
              </ListItem>

              {sample.status == "invalid" && sample.error &&
                <View>
                  <ListItem itemDivider>
                    <Text>Descrição do erro:</Text>
                  </ListItem>
                  <ListItem>
                    <Body>
                      <Text>
                        {sample.error.message} ({sample.error.code})
                      </Text>
                    </Body>
                  </ListItem>
                  <ListItem itemDivider>
                    <Text>Instruções para correção:</Text>
                  </ListItem>
                  <ListItem>
                    <Body>
                      <Text>
                        {this.fixInstructions(sample.error.code)}
                      </Text>
                    </Body>
                  </ListItem>
                </View>
              }

              {
                analisysDuration &&
                <View>
                  <ListItem itemDivider>
                    <Text>Duração da análise:</Text>
                  </ListItem>
                  <ListItem icon>
                    <Body>
                      <Text>{analisysDuration}</Text>
                    </Body>
                  </ListItem>
                </View>
              }

            </List>
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    sample: state.samples[ownProps.sampleId]
  };
};

const mapDispatchToProps = {
  created
};

export default connect(mapStateToProps, mapDispatchToProps)(SampleView);
