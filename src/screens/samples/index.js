import React from "react";
import { connect } from "react-redux";

// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Style
import { navigatorStyle } from "styles";

// Components
import {
  Body,
  Container,
  Content,
  Icon,
  Text,
  List,
  ListItem,
  Right
} from "native-base";
import { Chart } from "components";

// Actions
import { findSamples } from "actions/samples";
import { findCities } from "actions/cities";
import navigationOptions from "../navigationOptions";

class SampleList extends React.Component {
  static navigatorStyle = {
    ...navigatorStyle,
    tabBarHidden: true
  };

  constructor(props) {
    super(props);

    // Navigator
    this.props.navigator.addOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    // bindings
    this.showSample = this.showSample.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.id == "willAppear") {
      this.props.findSamples({
        trapId: this.props.trap.id,
        $sort: {
          collectedAt: -1
        }
      });
      this.props.findCities({
        id: this.props.trap.cityId
      });
    }
  }

  showSample(sampleId) {
    this.props.navigator.push({
      ...navigationOptions("aetrapp.Samples.Show"),
      passProps: {
        sampleId
      }
    });
  }

  render() {
    const { samples, city } = this.props;

    return (
      <Container>
        <Content>
          {
            samples && city && city.eggCountAverages && <Chart samples={samples} city={city} />
          }
          <ListItem itemDivider>
            <Text>Envios:</Text>
          </ListItem>
          <List
            dataArray={samples}
            renderRow={sample => (
              <ListItem icon button onPress={() => this.showSample(sample.id)}>
                <Body>
                  <Text>{moment(sample.collectedAt).format('DD/MM/YYYY')}</Text>
                </Body>
                <Right>
                  <Text>
                    {sample.eggCount !== null ? sample.eggCount + ' ovos' : 'não analisada'}
                  </Text>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>
            )}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {

  let samples = _.filter(state.samples, { "trapId": ownProps.trapId });
  samples = _.castArray(samples);
  samples = _.orderBy(samples, 'collectedAt', 'desc');

  const trap = state.traps[ownProps.trapId];

  return {
    samples,
    trap,
    city: state.cities[trap.cityId]
  };
};

const mapDispatchToProps = {
  findSamples,
  findCities
};

export default connect(mapStateToProps, mapDispatchToProps)(SampleList);
