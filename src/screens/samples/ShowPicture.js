import { connect } from "react-redux";
import React, { Component } from "react";

// App config
import config from "../../../config.json";

// Styles
import { navigatorStyle } from "styles";

// Components
import { View, Dimensions } from "react-native";
import Image from "react-native-transformable-image-next";

class ShowSamplePicture extends React.Component {
  static navigatorStyle = {
    navBarTransparent: true,
    navBarButtonColor: "white",    
    topBarElevationShadowEnabled: false,
    tabBarHidden: true
  };

  constructor(props) {
    super(props);

    // Navigation
    this.onLayout = this.onLayout.bind(this);

    // State
    const { width, height } = Dimensions.get("window");
    this.state = {
      width,
      height
    };
  }

  onLayout() {
    const { width, height } = Dimensions.get("window");
    this.setState({
      width,
      height
    });
  }


  render() {
    const { sample } = this.props;
    const { width, height } = this.state;

    const uri = `${config.API_URL}/files/${sample.blobId}`;

    return (
      <View style={{ backgroundColor: "black" }} onLayout={this.onLayout}>
        <Image
          style={{ width: width, height: height }}
          source={{ uri }}
        />
      </View>
    );
  }
}

export default ShowSamplePicture;
