import { PermissionsAndroid, Platform } from 'react-native';
import { setLocation } from "actions/location";

export default async function initWatchPosition(dispatch) {

  if (Platform.OS == 'ios') {
    initWatchPosition(dispatch);
    return;
  }

  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      startListener(dispatch);
    } else {
      setTimeout(()=>{
        initWatchPosition(dispatch);
      }, 1000);
    }
  } catch (err) {
    // Wait and retry if Activity is not ready
    if (err.code == "E_INVALID_ACTIVITY") {
      setTimeout(()=>{
        initWatchPosition(dispatch);
      }, 1000);
    }
  }
}

function startListener(dispatch) {
  navigator.geolocation.watchPosition(
    position => {
      // update state with location
      dispatch(setLocation({
        isAvailable: true,
        lon: position.coords.longitude,
        lat: position.coords.latitude
      }));
    },
    error => {
      // Do not log in production and when GPS is not available (error 3)
      if (__DEV__ && error.code != 3) console.log(error.message); // eslint-disable-line    
    },
    {
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 1000
    }
  );
}