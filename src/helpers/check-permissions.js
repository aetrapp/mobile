import { PermissionsAndroid, Platform } from 'react-native';

export default async function checkPermissions() {

  if (Platform.OS == 'ios') return;
  
  await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
  await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
  await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
  await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  
}