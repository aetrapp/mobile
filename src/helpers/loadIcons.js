import { Platform } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";

// import colors
import { colors } from "styles";

// loadIcons expects a specific order for all icons above
export default function loadIcons() {
  return new Promise((resolve, reject) => {
    const icons = {
      android: [
        MaterialCommunityIcons.getImageSource("cup", 20, "green"),
        MaterialCommunityIcons.getImageSource("map-marker-multiple", 20, "green"),
        MaterialIcons.getImageSource("notifications", 20, "green"),
        MaterialIcons.getImageSource("import-contacts", 20, "green"),
        MaterialIcons.getImageSource("person", 20, "green"),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.black),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.blue),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.green),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.yellow),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.yellow),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.red),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.gray)
      ],
      ios: [
        MaterialCommunityIcons.getImageSource("cup", 20, "green"),
        MaterialCommunityIcons.getImageSource("map-marker-multiple", 20, "green"),
        MaterialIcons.getImageSource("notifications", 20, "green"),
        MaterialIcons.getImageSource("import-contacts", 20, "green"),
        MaterialIcons.getImageSource("person", 20, "green"),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.black),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.blue),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.green),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.yellow),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.yellow),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.red),
        MaterialCommunityIcons.getImageSource("map-marker", 40, colors.map.gray)
      ]
    };

    Promise.all(icons[Platform.OS])
      .then(loadedIcons => {
        resolve({
          home: loadedIcons[0],
          map: loadedIcons[1],
          bars: loadedIcons[2],
          help: loadedIcons[3],
          user: loadedIcons[4],
          "inactive": loadedIcons[5],
          "waiting-sample": loadedIcons[6],
          "no-eggs": loadedIcons[7],
          "below-average": loadedIcons[8],
          "on-average": loadedIcons[9],
          "above-average": loadedIcons[10],
          "delayed": loadedIcons[11]
        });
      })
      .catch(err => reject(err))
      .done();
  });
}
