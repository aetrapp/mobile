import { Alert } from "react-native";

export default function errorAlert(err, callback) {
  
  let title, message;

  if ((err.className && err.className == 'timeout') || (err.message && err.message == 'Socket connection timed out')) {
    title = "Erro de conexão";
    message = "Não foi possível acessar o serviço do AeTrapp.";
  } else if (err.className && err.className == 'not-authenticated') {
    title = "Autenticação inválida";
    message = "Houve um erro de autenticação do seu usuário e não foi possível completar a ação. Por favor, tente fazer login novamente.";
  } else {
    title = "Erro";
    message = err.message;
  }
  
  Alert.alert(title, message, [{ text: 'OK', onPress: callback }], { cancelable: false });
}
