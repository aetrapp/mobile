import moment from "moment";
import t from "tcomb-validation";
const { validate } = t;

/*
 * E-mail 
 */

const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
var EmailValidator = t.refinement(t.String, function (value) {
  return emailRegex.test(value);
});

EmailValidator.getValidationErrorMessage = function (value) {
  if (!value) {
    return 'Informe endereço de e-mail.';
  }

  if (!emailRegex.test(value)) {
    return 'O e-mail informado não é válido.';
  }
};

/*
 * Password
 */

var PasswordValidator = t.refinement(t.String, function (value) {
  return value && value.length >= 6 && value.length < 50;
});

PasswordValidator.getValidationErrorMessage = function (value) {
  if (!value || value.length == 0) {
    return 'Informe a senha.';
  }

  if (value.length < 6) {
    return 'A senha informada deve ter 6 ou mais caracteres.';
  }
};

/*
 * First name
 */

var FirstNameValidator = t.refinement(t.String, function (value) {
  return value && value.length > 0;
});

FirstNameValidator.getValidationErrorMessage = function (value) {
  if (!value || value.length == 0) {
    return 'Informe seu nome.';
  }
};

/*
 * Last name
 */

var LastNameValidator = t.refinement(t.String, function (value) {
  return value && value.length > 0;
});

LastNameValidator.getValidationErrorMessage = function (value) {
  if (!value || value.length == 0) {
    return 'Informe seu sobrenome.';
  }
};

/*
 * Cellphone number
 */

var CellphoneValidator = t.refinement(t.String, function (value) {
  return value && value.length > 0;
});

CellphoneValidator.getValidationErrorMessage = function (value) {
  if (!value || value.length == 0) {
    return 'Informe o número de celular.';
  }
};

/*
 * Date (format is DD/MM/YYYY)
 */

var BrDateValidator = t.refinement(t.String, function (value) {
  const brDate = moment(value, 'DD-MM-YYYY');
  return value && value.length > 0 && brDate.isValid();
});

BrDateValidator.getValidationErrorMessage = function (value) {
  if (!value || value.length == 0) {
    return 'Informe a data de nascimento.';
  }

  const brDate = moment(value, 'DD-MM-YYYY');
  if (!brDate.isValid()) {
    return 'Data inválida (Use o formato DD/MM/AAAA)';
  }
  return ;
};

module.exports = {
  validateEmail: value => {
    return validate(value || '', EmailValidator);
  },
  validatePassword: value => {
    return validate(value || '', PasswordValidator);
  },
  validateFirstName: value => {
    return validate(value || '', FirstNameValidator);
  },
  validateLastName: value => {
    return validate(value || '', LastNameValidator);
  },
  validateCellphone: value => {
    return validate(value || '', CellphoneValidator);
  },
  validateBrDate: value => {
    return validate(value || '', BrDateValidator);
  }
};