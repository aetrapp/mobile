export default function translateGender(gender) {
  switch (gender) {
    case "female":
      return "Feminino";
    case "male":
      return "Masculino";
    case "other":
      return "Outro";
    default:
      return "Não informado";
  }
}