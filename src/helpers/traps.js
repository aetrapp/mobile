// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Style
import { colors } from "styles";

export function getStatus(trap) {
  const { image } = trap;
  if (!image) {
    return "Coletando";
  } else {
    if (!image.processed) {
      return "Processando";
    } else {
      if (image.error) {
        return "Falhou";
      } else {
        return "Concluída";
      }
    }
  }
}

export function setTrapStatusAndColor(trap) {

  // Calculate daysToCollect
  trap.age = moment().diff(trap.cycleStart, 'days', true);
  trap.windowStart = moment(trap.cycleStart).add(trap.cycleDuration - 1, 'days').fromNow();
  trap.daysToCollect = trap.cycleDuration - trap.age - 1;

  const statusMessages = {
    "inactive": {
      text: "Desativada",
      color: colors.map.black
    },
    "waiting-sample": {
      text: "Aguardando amostra",
      color: colors.map.blue
    },
    "no-eggs": {
      text: "Sem ovos",
      color: colors.map.green
    },
    "above-average": {
      text: "Acima da média da cidade",
      color: colors.map.red
    },
    "below-average": {
      text: "Abaixo da média da cidade",
      color: colors.map.yellow
    },
    "delayed": {
      text: "Atrasada, descarte a amostra",
      color: colors.map.gray
    }
  }

  trap.statusMessage = statusMessages[trap.status] && statusMessages[trap.status].text;
  trap.color = statusMessages[trap.status] && statusMessages[trap.status].color;

  // Set samples to empty array when trap is new
  if (!trap.samples) trap.samples = [];

  return trap;
}