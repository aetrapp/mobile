import notifications from "services/notifications";
import samples from "services/samples";
import traps from "services/traps";
import user from "services/user";
import validateStore from "services/helpers/validate-store";

export default function init(store, callback) {
  let unsubscribe;
  unsubscribe = store.subscribe(() => {    
    if (store.getState().context.rehydrated) {
      unsubscribe();

      user(store);

      validateStore(store, "traps").then(removed => {
        traps(store).batchRemove(removed);
      });
      validateStore(store, "samples").then(removed => {
        samples(store).batchRemove(removed);
      });
      validateStore(store, "notifications").then(removed => {
        notifications(store).batchRemove(removed);
      });
      if (typeof callback == "function") callback();
    }
  });
}
