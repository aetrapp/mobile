import io from "socket.io-client";
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';
import { AsyncStorage } from "react-native";

import config from "../../../config.json";

const url = config.API_URL;
if (!url) throw new Error('API_URL must be defined.');

const socket = io(url, {
  transports: ["websocket"],
  forceNew: true
});

const client = feathers()
  .configure(socketio(socket))
  .configure(
    auth({
      storage: AsyncStorage
    })
  );

export default client;
