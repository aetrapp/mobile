import axios from "axios";
import config from "../../../config.json";

const httpClient = axios.create({
  baseURL: config.API_URL
});

export default function validateStore(store, path, reducer) {
  return new Promise((resolve, reject) => {
    reducer = reducer || path;
    const ids = Object.keys(store.getState()[reducer]).map(id => id);
    if (ids.length) {
      return httpClient
        .post("/" + path + "/validate_store", { ids: ids })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    } else {
      resolve(ids);
    }
  });
}
