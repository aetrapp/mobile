import { patched } from "actions/auth";
import Realtime from "services/helpers/realtime";

export default function init(store) {
  const realtime = new Realtime("users", {
    bindings: {
      patched: data => {
        store.dispatch(patched(data));
      },
    }
  });

  return {
    realtime
  };
}
