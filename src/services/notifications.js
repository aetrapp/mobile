import { created, removed } from "actions/notifications";
import Realtime from "services/helpers/realtime";

export default function init(store) {
  const realtime = new Realtime("notifications", {
    bindings: {
      created: data => {
        store.dispatch(created(data));
      },
      removed: data => {
        store.dispatch(removed(data));
      }
    }
  });

  const batchRemove = ids => {
    ids.forEach(id => {
      store.dispatch(removed({ id: id }));
    });
  };

  return {
    realtime,
    batchRemove
  };
}
