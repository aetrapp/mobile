import { created, patched, removed } from "actions/samples";
import { findTraps } from "actions/traps";
import Realtime from "services/helpers/realtime";

export default function init(store) {
  const realtime = new Realtime("samples", {
    bindings: {
      created: data => {
        store.dispatch(created(data));
        store.dispatch(findTraps({id: data.trapId}));
      },
      patched: data => {
        store.dispatch(patched(data));
        store.dispatch(findTraps({id: data.trapId}));
      },
      removed: data => {
        store.dispatch(removed(data));
        store.dispatch(findTraps({id: data.trapId}));
      }
    }
  });

  const batchRemove = ids => {
    ids.forEach(id => {
      store.dispatch(removed({ id: id }));
    });
  };

  return {
    realtime,
    batchRemove
  };
}
