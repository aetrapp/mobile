import client from "services/helpers/client";

export const CITIES_INIT = "CITIES_INIT";
export const CITIES_FIND_REQUEST = "CITIES_FIND_REQUEST";
export const CITIES_FIND_SUCCESS = "CITIES_FIND_SUCCESS";
export const CITIES_FIND_FAILURE = "CITIES_FIND_FAILURE";

const findRequest = query => {
  return {
    type: CITIES_FIND_REQUEST,
    query
  }
};

const findSuccess = (query, res) => {
  return {
    type: CITIES_FIND_SUCCESS,
    query,
    res
  }
};

const findFailure = (query, err) => {
  return {
    type: CITIES_FIND_FAILURE,
    query,
    err
  }
};

// Exposed actions

export const initCities = () => dispatch => {
  dispatch({
    type: CITIES_INIT
  });
};

export const findCities = (query = {}) => dispatch => {
  dispatch(findRequest(query));
  client.service("cities")
    .find({ query })
    .then(res => {
      dispatch(findSuccess(query, res));
    }).catch(err => {
      dispatch(findFailure(query, err));
    });
}
