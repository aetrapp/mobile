import client from "services/helpers/client";
const service = client.service("notifications");

/*
 * SOCKET EVENTS
 */

export const NOTIFICATION_NEW = "NOTIFICATION_NEW";

export const NOTIFICATION_FIND_FAILURE = "NOTIFICATION_FIND_FAILURE";
export const NOTIFICATION_FIND_SUCCESS = "NOTIFICATION_FIND_SUCCESS";
export const NOTIFICATION_FIND_REQUEST = "NOTIFICATION_FIND_REQUEST";

export const NOTIFICATION_GET_SUCCESS = "NOTIFICATION_GET_SUCCESS";

export const NOTIFICATION_REMOVE = "NOTIFICATION_REMOVE";
export const NOTIFICATION_REMOVE_SUCCESS = "NOTIFICATION_REMOVE_SUCCESS";

const _new = (id, data) => {
  return {
    type: NOTIFICATION_NEW,
    id,
    data
  };
};

const _remove = (id, data) => {
  return {
    type: NOTIFICATION_REMOVE,
    id,
    data
  };
};

export const created = data => dispatch => {
  return dispatch(_new(data.id, data));
};

export const removed = data => dispatch => {
  return dispatch(_remove(data.id, data));
};

const findRequest = query => {
  return {
    type: NOTIFICATION_FIND_REQUEST,
    query
  };
};

const findSuccess = (query, res) => {
  return {
    type: NOTIFICATION_FIND_SUCCESS,
    query,
    res
  };
};

const findFailure = (query, err) => {
  return {
    type: NOTIFICATION_FIND_FAILURE,
    query,
    err
  };
};

export const findNotifications = (query = {}) => dispatch => {
  dispatch(findRequest(query));
  service.find({
    query: Object.assign({}, query)
  }).then(res => {
    dispatch(findSuccess(query, res));
  }).catch(err => {
    dispatch(findFailure(query, err));
  });
};


const getSuccess = (id, data) => {
  return {
    type: NOTIFICATION_GET_SUCCESS,
    id,
    data
  };
};

export const getNotification = id => (dispatch) => {
  return service.get(id).then(res => {
    dispatch(getSuccess(id, res));
  });
};

const removeSuccess = id => {
  return {
    type: NOTIFICATION_REMOVE_SUCCESS,
    id
  };
};

export const removeNotification = id => dispatch => {
  return service.remove(id).then(()=>{    
    dispatch(removeSuccess(id));
  });
};
