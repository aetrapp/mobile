import client from "services/helpers/client";
const service = client.service("samples");

/*
 * ACTION NAMES
 */

export const SAMPLE_ASSISTANT_INIT = "SAMPLE_ASSISTANT_INIT";
export const SAMPLE_ASSISTANT_UPDATE_DATA = "SAMPLE_ASSISTANT_UPDATE_DATA";
export const SAMPLE_ASSISTANT_UPDATE_CONTEXT = "SAMPLE_ASSISTANT_UPDATE_CONTEXT";
export const SAMPLE_CREATE_REQUEST = 'SAMPLE_CREATE_REQUEST';
export const SAMPLE_CREATE_SUCCESS = 'SAMPLE_CREATE_SUCCESS';
export const SAMPLE_CREATE_FAILURE = 'SAMPLE_CREATE_FAILURE';

/*
 * REQUEST EVENTS
 */

const _createRequest = data => {
  return {
    type: SAMPLE_CREATE_REQUEST,
    data
  }
};

const _createSuccess = data => {
  return {
    type: SAMPLE_CREATE_SUCCESS,
    data
  }
};

const _createFailure = (data, err) => {
  return {
    type: SAMPLE_CREATE_FAILURE,
    data,
    err
  }
};

/*
 * EXPOSED ACTIONS
 */

export const initSampleAssistant = data => dispatch => {
  return dispatch({ type: SAMPLE_ASSISTANT_INIT, data });
};

export const updateData = data => dispatch => {
  dispatch({ type: SAMPLE_ASSISTANT_UPDATE_DATA, data });
};

export const updateContext = data => dispatch => {
  dispatch({ type: SAMPLE_ASSISTANT_UPDATE_CONTEXT, data });
};
 
export const createSample = data => (dispatch) => {
  dispatch(_createRequest(data));
  service.timeout = 60000;
  return service.create(data).then(card => {
    return dispatch(_createSuccess(card));
  });
};
