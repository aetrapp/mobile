export const LOCATION_SET = 'LOCATION_SET';

export const setLocation = location => dispatch => {
  return dispatch({
    type: LOCATION_SET,
    location
  });
};
