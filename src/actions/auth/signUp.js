export const SIGN_UP_INIT = "SIGN_UP_INIT";
export const SIGN_UP_UPDATE_DATA = "SIGN_UP_UPDATE_DATA";

export const initSignUp = data => dispatch => {
  dispatch({ type: SIGN_UP_INIT, data });
};

export const updateSignUpData = data => dispatch => {
  dispatch({ type: SIGN_UP_UPDATE_DATA, data });
};
