import _ from "lodash";

import client from "services/helpers/client";
const service = client.service("traps");

import { findCities } from "actions/cities";

/*
 * SOCKET EVENTS
 */

export const TRAP_NEW = "TRAP_NEW";
export const TRAP_PATCH = "TRAP_PATCH";
export const TRAP_REMOVE = "TRAP_REMOVE";
export const TRAP_FIND_REQUEST = "TRAP_FIND_REQUEST";
export const TRAP_FIND_SUCCESS = "TRAP_FIND_SUCCESS";
export const TRAP_FIND_FAILURE = "TRAP_FIND_FAILURE";
export const TRAP_CREATE_REQUEST = "TRAP_CREATE_REQUEST";
export const TRAP_CREATE_SUCCESS = "TRAP_CREATE_SUCCESS";
export const TRAP_CREATE_FAILURE = "TRAP_CREATE_FAILURE";
export const TRAP_PATCH_REQUEST = "TRAP_PATCH_REQUEST";
export const TRAP_PATCH_SUCCESS = "TRAP_PATCH_SUCCESS";
export const TRAP_PATCH_FAILURE = "TRAP_PATCH_FAILURE";

const _new = (id, data) => {
  return {
    type: TRAP_NEW,
    id,
    data
  };
};

const _patch = (id, data) => {
  return {
    type: TRAP_PATCH,
    id,
    data
  };
};

const _remove = (id, data) => {
  return {
    type: TRAP_REMOVE,
    id,
    data
  };
};

export const created = data => dispatch => {
  return dispatch(_new(data.id, data));
};

export const patched = data => dispatch => {
  return dispatch(_patch(data.id, data));
};

export const removed = data => dispatch => {
  return dispatch(_remove(data.id, data));
};

const findRequest = query => {
  return {
    type: TRAP_FIND_REQUEST,
    query
  };
};

const findSuccess = (query, res) => {
  return {
    type: TRAP_FIND_SUCCESS,
    query,
    res
  };
};

const findFailure = (query, err) => {
  return {
    type: TRAP_FIND_FAILURE,
    query,
    err
  };
};

const createRequest = (query) => {
  return {
    type: TRAP_CREATE_REQUEST,
    query
  };
};

const createSuccess = (query, res) => {
  return {
    type: TRAP_CREATE_SUCCESS,
    data: res
  };
};

const createFailure = (query, err) => {
  return {
    type: TRAP_CREATE_FAILURE,
    query,
    err
  };
};

const patchRequest = (id, query) => {
  return {
    type: TRAP_PATCH_REQUEST,
    id,
    query
  };
};

const patchSuccess = (id, query, res) => {
  return {
    type: TRAP_PATCH_SUCCESS,
    data: res
  };
};

const patchFailure = (id, query, err) => {
  return {
    type: TRAP_PATCH_FAILURE,
    id,
    query,
    err
  };
};

export const findTraps = (query = {}) => dispatch => {
  dispatch(findRequest(query));
  client.service("traps").find({
    query: Object.assign({}, query)
  }).then(res => {
    dispatch(findSuccess(query, res));

    // update cities
    let citiesIds = res.data.map(trap => {
      return trap.cityId;
    });
    citiesIds = _.uniq(citiesIds);
    dispatch(findCities({ id: { $in: citiesIds } }));

  }).catch(err => {
    dispatch(findFailure(query, err));
  });
};

export const createTrap = query => dispatch => {
  dispatch(createRequest(query));
  service.timeout = 60000;
  return service
    .create(query)
    .then(res => {
      dispatch(createSuccess(query, res));
    }).catch(err => {
      dispatch(createFailure(query, err));
      throw err;
    });
};

export const patchTrap = (id, query) => dispatch => {
  dispatch(patchRequest(id, query));
  return service
    .patch(id, query)
    .then(res => {
      dispatch(patchSuccess(id, query, res));
    }).catch(err => {
      dispatch(patchFailure(id, query, err));
      throw err;
    });
};

export const removeTrap = id => () => {
  return client.service("traps").remove(id);
};