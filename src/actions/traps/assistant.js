import _ from "lodash";
import { states } from "collections/states.json";
import { cities } from "collections/cities.json";

import client from "services/helpers/client";
const service = client.service("traps");

import axios from "axios";
const httpClient = axios.create();

// getAddress

export const TRAP_ASSISTANT_GET_ADDRESS_REQUEST = "TRAP_ASSISTANT_GET_ADDRESS_REQUEST";
const _getAddressRequest = (status, data) => {
  return {
    type: TRAP_ASSISTANT_GET_ADDRESS_REQUEST,
    status,
    data
  };
};

export const TRAP_ASSISTANT_GET_ADDRESS_SUCCESS = "TRAP_ASSISTANT_GET_ADDRESS_SUCCESS";
const _getAddressSuccess = (status, data) => {
  return {
    type: TRAP_ASSISTANT_GET_ADDRESS_SUCCESS,
    status,
    data
  };
};

export const TRAP_ASSISTANT_GET_ADDRESS_FAILURE = "TRAP_ASSISTANT_GET_ADDRESS_FAILURE";
const _getAddressFailure = (status, data) => {
  return {
    type: TRAP_ASSISTANT_GET_ADDRESS_FAILURE,
    status,
    data
  };
};

export const getAddress = mapRegion => dispatch => {
  const query = Object.assign(
    {},
    {
      format: "json",
      "accept-language": "pt",
      addressdetails: 1,
      zoom: 18,
      lat: mapRegion.latitude,
      lon: mapRegion.longitude,
      email: "contato@aparabolica.com.br"
    }
  );

  dispatch(_getAddressRequest({ isFetchingAddress: true }));
  httpClient
    .get("http://nominatim.openstreetmap.org/reverse", {
      params: query
    })
    .then(function (res) {
      const { state, city, suburb, road } = res.data.address;
      const stateId = _.find(states, ["name", state]).id;
      const cityId = _.find(cities, {
        stateId,
        name: city
      }).id;

      dispatch(
        _getAddressSuccess({
          isFetchingAddress: false
        }, {
            stateId,
            cityId,
            neighbourhood: suburb,
            addressStreet: road

          })
      );
    })
    .catch(function (err) {
      dispatch(_getAddressFailure({ isFetchingAddress: false }));
    });
};

// initCreateTrapAssistant
export const TRAP_ASSISTANT_INIT = "TRAP_ASSISTANT_INIT";
export const initCreateTrapAssistant = () => dispatch => {
  return dispatch({ type: TRAP_ASSISTANT_INIT });
};

// updateCreateAssistant
export const TRAP_ASSISTANT_UPDATE_DATA = "TRAP_ASSISTANT_UPDATE_DATA";
export const updateCreateAssistant = data => dispatch => {
  dispatch({ type: TRAP_ASSISTANT_UPDATE_DATA, data });
};