// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Service
import client from "services/helpers/client";
const userService = client.service("users");

// Notifications
import { oneSignalAppId } from "../../config.json";
import OneSignal from 'react-native-onesignal';
OneSignal.init(oneSignalAppId);

// SIGN UP actions

export const AUTH_SIGN_UP_REQUEST = "AUTH_SIGN_UP_REQUEST";
export const AUTH_SIGN_UP_SUCCESS = "AUTH_SIGN_UP_SUCCESS";
export const AUTH_SIGN_UP_FAILURE = "AUTH_SIGN_UP_FAILURE";

const _signUpRequest = credentials => {
  return {
    type: AUTH_SIGN_UP_REQUEST,
    credentials
  };
};

export const signUp = userData => dispatch => {
  userData.dateOfBirth = moment(userData.dateOfBirth, 'DD/MM/YYYY').toDate();
  dispatch(_signUpRequest(userData));
  return userService
    .create(userData);
};

// SIGN IN actions

export const AUTH_SIGN_IN_REQUEST = "AUTH_SIGN_IN_REQUEST";
export const AUTH_SIGN_IN_SUCCESS = "AUTH_SIGN_IN_SUCCESS";
export const AUTH_SIGN_IN_FAILURE = "AUTH_SIGN_IN_FAILURE";

const _signInRequest = query => {
  return {
    type: AUTH_SIGN_IN_REQUEST,
    query
  };
};
const _signInSuccess = (accessToken, user) => {
  return {
    type: AUTH_SIGN_IN_SUCCESS,
    accessToken,
    user
  };
};
const _signInFailure = (query, err) => {
  return {
    type: AUTH_SIGN_IN_FAILURE,
    query,
    err
  };
};

export const signIn = credentials => dispatch => {
  // for dev: use the strategy below when endpoint has changed
  // let query = {
  //   strategy: 'jwt'
  // };
  let query;
  if (typeof credentials !== 'undefined') {
    query = Object.assign(
      {
        strategy: "local"
      },
      credentials
    );
  }

  let accessToken;
  dispatch(_signInRequest(query));
  return client
    .authenticate(query)
    .then(response => {
      accessToken = response.accessToken;
      return client.passport.verifyJWT(response.accessToken);
    })
    .then(payload => {
      return client.service("users").get(payload.userId);
    })
    .then(user => {
      OneSignal.sendTag("userId", user.id);
      dispatch(_signInSuccess(accessToken, user));
    })
    .catch(err => {
      dispatch(_signInFailure(query, err));
      throw err;
    });
};

// SIGN OUT

export const AUTH_SIGN_OUT = "AUTH_SIGN_OUT";

export const signOut = () => dispatch => {
  return client.logout().then(() => {
    OneSignal.deleteTag("userId");
    dispatch({ type: AUTH_SIGN_OUT });
  });
};

// Update credentials
export const AUTH_UPDATE_CREDENTIALS = "AUTH_UPDATE_CREDENTIALS";
export const updateCredentials = data => dispatch => {
  dispatch({ type: AUTH_UPDATE_CREDENTIALS, data });
};

// Update profile
export const AUTH_UPDATE_PROFILE = "AUTH_UPDATE_PROFILE";
export const updateProfile = data => dispatch => {
  dispatch({ type: AUTH_UPDATE_PROFILE, data });
};

// PATCH PROFILE

export const AUTH_PATCH_PROFILE_REQUEST = "AUTH_PATCH_PROFILE_REQUEST";
export const AUTH_PATCH_PROFILE_SUCCESS = "AUTH_PATCH_PROFILE_SUCCESS";
export const AUTH_PATCH_PROFILE_FAILURE = "AUTH_PATCH_PROFILE_FAILURE";

const _patchProfileRequest = query => {
  return {
    type: AUTH_PATCH_PROFILE_REQUEST,
    query
  };
};
const _patchProfileSuccess = (id, data) => {
  return {
    type: AUTH_PATCH_PROFILE_SUCCESS,
    id,
    data
  };
};
const _patchProfileFailure = (query, err) => {
  return {
    type: AUTH_PATCH_PROFILE_FAILURE,
    query,
    err
  };
};

export const patched = data => dispatch => {
  return dispatch(_patchProfileSuccess(data.id, data));
};

export const patchProfile = (id, data) => dispatch => {
  const query = Object.assign({}, data);
  query.dateOfBirth = moment(query.dateOfBirth, 'DD/MM/YYYY').format('YYYY-MM-DD');

  dispatch(_patchProfileRequest(id, query));
  return userService
    .patch(id, query)
    .then(res => {
      dispatch(_patchProfileSuccess(id, res));
    }).catch(err => {
      dispatch(_patchProfileFailure(id, query, err));
      throw err;
    });
};