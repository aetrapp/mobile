import client from "services/helpers/client";
const service = client.service("samples");

/*
 * ACTION NAMES
 */

export const SAMPLE_NEW = "SAMPLE_NEW";
export const SAMPLE_UPDATE = "SAMPLE_UPDATE";
export const SAMPLE_REMOVE = "SAMPLE_REMOVE";
export const SAMPLE_FIND_REQUEST = "SAMPLE_FIND_REQUEST";
export const SAMPLE_FIND_SUCCESS = "SAMPLE_FIND_SUCCESS";
export const SAMPLE_FIND_ERROR = "SAMPLE_FIND_ERROR";

/*
 * SOCKET EVENTS
 */

const _new = (id, data) => {
  return {
    type: SAMPLE_NEW,
    id,
    data
  };
};

const _update = (id, data) => {
  return {
    type: SAMPLE_UPDATE,
    id,
    data
  };
};

const _remove = (id, data) => {
  return {
    type: SAMPLE_REMOVE,
    id,
    data
  };
};

/*
 * REQUEST EVENTS
 */

const findRequest = query => {
  return {
    type: SAMPLE_FIND_REQUEST,
    query
  }
};

const findSuccess = (query, res) => {
  return {
    type: SAMPLE_FIND_SUCCESS,
    query,
    res
  }
};

const findFailure = (query, err) => {
  return {
    type: SAMPLE_FIND_ERROR,
    query,
    err
  }
};

/*
 * EXPOSED ACTIONS
 */

export const created = data => dispatch => {
  return dispatch(_new(data.id, data));
};

export const patched = data => dispatch => {
  return dispatch(_update(data.id, data));
};

export const removed = data => dispatch => {
  return dispatch(_remove(data.id, data));
};

export const findSamples = query => dispatch => {
  dispatch(findRequest(query));
  service
    .find({ query })
    .then(res => {
      dispatch(findSuccess(query, res));
    }).catch(err => {
      dispatch(findFailure(query, err));
    });
}