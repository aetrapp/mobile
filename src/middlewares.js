import reduxThunk from "redux-thunk";
import reduxPromiseMiddleware from "redux-promise-middleware";
import logger from "redux-logger";

let middlewares = [
  reduxThunk,
  reduxPromiseMiddleware()
];

if (__DEV__ === true) {
  middlewares.push(logger);
}

export default middlewares;




