
// Set base colors
let colors = {
  // primary
  greenPea: "rgb(24,89,52)",
  apple: "rgb(109,188,68)",
  // secondary
  neonCarrot: "rgb(255,144,46)",
  sunglow: "rgb(255,189,46)",
  emerald: 'rgb(88,208,181)',
  // text
  tundora: "rgb(70,70,70)",
  silver: "rgb(200,200,200)"
};

// Spinner
colors.spinner = {
  color: colors.apple,
  text: colors.apple,
  overlayColor: "#fff"
};

// https://github.com/react-community/react-native-maps/issues/887
colors.map = {
  black: "#010101",
  gray: "#818181",
  blue: "#006AB8",
  green: "#059C3F",
  red: "#EB604D",
  yellow: "#FAEC01"
};

let navigatorStyle = {
  navBarBackgroundColor: colors.tundora,
  navBarButtonColor: colors.apple,
  navBarButtonFontSize: 30,
  navBarNoBorder: true,
  navBarTextColor: colors.apple,
  topBarElevationShadowEnabled: false,
};

module.exports = { 
  colors, 
  navigatorStyle 
};