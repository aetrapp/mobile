import {
  NOTIFICATION_NEW,
  NOTIFICATION_GET_SUCCESS,
  NOTIFICATION_FIND_SUCCESS,
  NOTIFICATION_REMOVE,
  NOTIFICATION_REMOVE_SUCCESS
} from 'actions/notifications';
import { AUTH_SIGN_OUT } from 'actions/auth';

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case NOTIFICATION_NEW: 
  case NOTIFICATION_GET_SUCCESS: {
    let notification = { ...action.data };
    return {
      ...state,
      [action.data.id]: notification
    };
  }
  case NOTIFICATION_FIND_SUCCESS: {
    state = { ...state };

    action.res.data.forEach(notification => {
      state[notification.id] = notification;
    });
    return state;
  }
  case NOTIFICATION_REMOVE:
  case NOTIFICATION_REMOVE_SUCCESS: {
    if (state[action.id]) {
      state = { ...state };
      delete state[action.id];
    }
    return state;
  }
  case AUTH_SIGN_OUT:
    return initialState;
  default: {
    return state;
  }
  }
}
