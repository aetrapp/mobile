import { CONTEXT_UPDATE } from "actions/context";

import { REHYDRATE } from "redux-persist/constants";

const initialState = {
  satelliteBaseLayer: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CONTEXT_UPDATE: {
      return {
        ...state,
        [action.context]:
          typeof action.data == "object"
            ? {
              ...state[action.context],
              ...action.data
            }
            : action.data
      };
    }
    case REHYDRATE: {
      return {
        ...action.payload.context,
        rehydrated: true
      };
    }
    default:
      return state;
  }
}
