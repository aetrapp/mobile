import {
  SIGN_UP_INIT,
  SIGN_UP_UPDATE_DATA,
} from "actions/auth/signUp";
import { AUTH_SIGN_OUT } from 'actions/auth';

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SIGN_UP_INIT:
      return { 
        ...initialState,
        ...action.data
      };
    case SIGN_UP_UPDATE_DATA:
      return {
        ...state,
        ...action.data
      }
    case AUTH_SIGN_OUT:
      return initialState;      
    default: 
      return state;    
  }
}