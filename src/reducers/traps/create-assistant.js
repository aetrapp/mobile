
import {
  TRAP_ASSISTANT_INIT,
  TRAP_ASSISTANT_CREATE_REQUEST,
  TRAP_ASSISTANT_CREATE_FAILURE,
  TRAP_ASSISTANT_CREATE_SUCCESS,
  TRAP_ASSISTANT_GET_ADDRESS_FAILURE,
  TRAP_ASSISTANT_GET_ADDRESS_REQUEST,
  TRAP_ASSISTANT_GET_ADDRESS_SUCCESS,
  TRAP_ASSISTANT_UPDATE_DATA,
} from "actions/traps/assistant";
import { AUTH_SIGN_OUT } from 'actions/auth';

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TRAP_ASSISTANT_INIT:
      return {
        ...initialState
      };
    case TRAP_ASSISTANT_UPDATE_DATA:
      return {
        ...state,
        ...action.data
      }
    case TRAP_ASSISTANT_GET_ADDRESS_REQUEST:
    case TRAP_ASSISTANT_GET_ADDRESS_FAILURE:
    case TRAP_ASSISTANT_GET_ADDRESS_SUCCESS:
      return {
        ...state,
        ...action.status,
        data: {
          ...state.data,
          ...action.data
        }
      }
    case AUTH_SIGN_OUT:
      return initialState;      
    default:
      return state;
  }
}