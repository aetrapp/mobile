import _ from "lodash";

// Localized moment.js
import moment from "moment/min/moment-with-locales";
moment.locale("pt-br");

// Actions
import { CITIES_FIND_SUCCESS } from 'actions/cities';
import {
  TRAP_NEW,
  TRAP_PATCH,
  TRAP_PATCH_SUCCESS,
  TRAP_FIND_SUCCESS,
  TRAP_REMOVE,
  TRAP_REMOVE_SUCCESS
} from 'actions/traps';
import { AUTH_SIGN_OUT } from 'actions/auth';

// Helpers
import { setTrapStatusAndColor } from "helpers/traps";

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TRAP_NEW:
    case TRAP_PATCH:
    case TRAP_PATCH_SUCCESS: {
      let trap = { ...action.data };
      const currentTrap = state[action.data.id];
      setTrapStatusAndColor(trap);
      return {
        ...state,
        [action.data.id]: {
          ...currentTrap, // merge current trap to avoid losing samples reference
          ...trap
        }
      };
    }
    case TRAP_FIND_SUCCESS: {
      state = { ...state };

      action.res.data.forEach(trap => {
        setTrapStatusAndColor(trap);
        state[trap.id] = trap;
      });
      return state;
    }
    case TRAP_REMOVE:
    case TRAP_REMOVE_SUCCESS: {
      if (state[action.id]) {
        state = { ...state };
        delete state[action.id];
      }
      return state;
    }
    case CITIES_FIND_SUCCESS: {
      // Update trap related to city retrieved
      state = { ...state };

      // For each city
      action.res.data.forEach(city => {
        const cityTraps = _.filter(state, { cityId: city.id });
        cityTraps.forEach(trap => {
          trap.city = city;
          trap = setTrapStatusAndColor(trap);
          state[trap.id] = trap;
        });
      });
      return state;
    }
    case AUTH_SIGN_OUT:
      return initialState;    
    default: {
      return state;
    }
  }
}
