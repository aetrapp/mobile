import { combineReducers } from "redux";
import auth from "./auth";
import cities from "./cities";
import context from "./context";
import location from "./location";
import notifications from "./notifications";
import sampleAssistant from "./sampleAssistant";
import samples from "./samples";
import signUp from "./signUp";
import trapCreateAssistant from "./traps/create-assistant";
import traps from "./traps";

export default function () {
  return combineReducers({
    auth,
    cities,
    context,
    location,
    notifications,
    sampleAssistant,
    samples,
    signUp,
    trapCreateAssistant,
    traps
  });
}
