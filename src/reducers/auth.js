import {
  AUTH_CHECK_SIGN_UP_REQUEST,
  AUTH_CHECK_SIGN_UP_SUCCESS,
  AUTH_CHECK_SIGN_UP_FAILURE,
  AUTH_SIGN_IN_REQUEST,
  AUTH_SIGN_IN_SUCCESS,
  AUTH_SIGN_IN_FAILURE,
  AUTH_SIGN_UP_REQUEST,
  AUTH_SIGN_UP_SUCCESS,
  AUTH_SIGN_UP_FAILURE,
  AUTH_SIGN_OUT,
  AUTH_UPDATE_PROFILE,
  AUTH_PATCH_PROFILE_SUCCESS,
  AUTH_UPDATE_CREDENTIALS
} from "actions/auth";

const initialState = {
  loading: false,
  signedUp: false,
  signedIn: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case AUTH_UPDATE_CREDENTIALS:
      return {
        ...state,
        credentials: {
          ...state.credentials,
          ...action.data
        }
      };
    case AUTH_UPDATE_PROFILE:
    case AUTH_PATCH_PROFILE_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.data
        }
      };
    case AUTH_CHECK_SIGN_UP_REQUEST:
      delete state.credentials;
      return Object.assign(
        {}, 
        state, 
        { loading: true }
      );
    case AUTH_CHECK_SIGN_UP_FAILURE:
      return Object.assign({}, state, { loading: false });
    case AUTH_CHECK_SIGN_UP_SUCCESS: {
      return Object.assign({}, state, {
        loading: false,
        credentials: {
          email: action.query.email
        },
        signedUp: action.res.total === 1 // true if e-mail was found
      });
    }
    case AUTH_SIGN_UP_REQUEST:
    case AUTH_SIGN_IN_REQUEST:
      return Object.assign({}, state, { loading: true });
    case AUTH_SIGN_UP_FAILURE:
    case AUTH_SIGN_IN_FAILURE: {
      const newState = Object.assign({}, state, {
        signedIn: false,
        signedUp: false,
        user: false,
        loading: false
      });
      delete newState.accessToken;
      return newState;
    }
    case AUTH_SIGN_UP_SUCCESS:
    case AUTH_SIGN_IN_SUCCESS: {
      return Object.assign({}, state, {
        loading: false,
        signedIn: true,
        user: action.user,
        accessToken: action.accessToken
      });
    }
    case AUTH_SIGN_IN_FAILURE: // eslint-disable-line
    case AUTH_SIGN_OUT: {
      const newState = {
        ...state, 
        signedIn: false,
        signedUp: false,
        user: false
      };
      delete newState.accessToken;
      return newState;
    }
    default:
      return state;
  }
}
