import {
  SAMPLE_ASSISTANT_INIT,
  SAMPLE_ASSISTANT_UPDATE_DATA,
  SAMPLE_ASSISTANT_UPDATE_CONTEXT,
  SAMPLE_ASSISTANT_CREATE_REQUEST,
  SAMPLE_ASSISTANT_CREATE_FAILURE,
  SAMPLE_ASSISTANT_CREATE_SUCCESS,
} from "actions/sampleAssistant";
import { AUTH_SIGN_OUT } from 'actions/auth';

const initialState = {
  isPosting: false,
  loadingFromGallery: false,
  data: {}
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SAMPLE_ASSISTANT_INIT:
      return { 
        ...initialState,
        data: action.data
      };
    case SAMPLE_ASSISTANT_UPDATE_CONTEXT:
      return {
        ...state,
        ...action.data
      }
    case SAMPLE_ASSISTANT_UPDATE_DATA:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.data
        }
      }
    case SAMPLE_ASSISTANT_CREATE_REQUEST:
    case SAMPLE_ASSISTANT_CREATE_FAILURE:
    case SAMPLE_ASSISTANT_CREATE_SUCCESS:
      return {
        ...state,
        ...action.status,
        data: {
          ...state.data,
          ...action.data
        }        
      }
    case AUTH_SIGN_OUT:
      return initialState;      
    default: 
      return state;    
  }
}