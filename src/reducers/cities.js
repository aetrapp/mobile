// data collection for pre-loading
import { cities } from "collections/cities.json";

// actions
import { CITIES_INIT, CITIES_FIND_SUCCESS } from 'actions/cities';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case CITIES_INIT: {
      state = cities;
      return state;
    }
    case CITIES_FIND_SUCCESS: {
      state = { ...state };
      action.res.data.forEach(item => {
        state[item.id] = item;
      });
      return state;
    }    
    default: {
      return state;
    }
  }
}
