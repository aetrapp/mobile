import { LOCATION_SET } from "actions/location";

export default function reducer(state = {}, action) {
  switch (action.type) {
    case LOCATION_SET:
      return action.location;
    default:
      return state;
  }
}
