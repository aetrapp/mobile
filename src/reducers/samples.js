import {
  SAMPLE_NEW,
  SAMPLE_UPDATE,
  SAMPLE_REMOVE,
  SAMPLE_FIND_SUCCESS,
} from 'actions/samples';
import { AUTH_SIGN_OUT } from 'actions/auth';

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SAMPLE_NEW:
    case SAMPLE_UPDATE: {
      return {
        ...state,
        [action.data.id]: action.data
      };
    }
    case SAMPLE_FIND_SUCCESS: {
      state = { ...state };

      action.res.data.forEach(sample => {
        state[sample.id] = sample;
      });
      return state;
    }
    case SAMPLE_REMOVE: { 
      if (state[action.id]) {
        state = { ...state };
        delete state[action.id];
      }
      return state;
    }
    case AUTH_SIGN_OUT:
      return initialState;
    default: {
      return state;
    }
  }
}
