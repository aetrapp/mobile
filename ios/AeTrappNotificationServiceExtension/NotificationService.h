//
//  NotificationService.h
//  AeTrappNotificationServiceExtension
//
//  Created by VG on 10/07/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
